package cn.tml.innermost.admin.controller;

import cn.tml.innermost.admin.entity.vos.AdminListVO;
import cn.tml.innermost.admin.entity.vos.AdminRoleVO;
import cn.tml.innermost.admin.entity.vos.AdminVO;
import cn.tml.innermost.admin.utils.AdminTokenGenerate;
import cn.tml.innermost.framework.security.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;

import cn.tml.innermost.admin.service.AdminService;
import cn.tml.innermost.admin.entity.dos.Admin;

import java.util.ArrayList;
import java.util.List;

/**
* <p>
    *  前端控制器
    * </p>
*
* @author SHIJINTAO
* @since 2022-10-16
*/

    @RestController
@RequestMapping("listener/admin")
        public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminTokenGenerate adminTokenGenerate;

    @PostMapping(value = "list")
    public ResultMessage listAdmin() {
    return ResultUtil.success();
    }

    /**
     * 管理员登录
     * @param adminVO 管理员登录参数
     * @param uuid 设备号
     * @return Token
     */
    @PostMapping(value = "login")
    public ResultMessage login(AdminVO adminVO, @RequestHeader String uuid){
        Admin admin = new Admin();
        admin.setAdminName(adminVO.getAdminName());
        admin.setId(1);
        Token token = adminTokenGenerate.createToken(admin, true);
        return ResultUtil.data(token);
    }

    /**
     * 新增管理员
     * @param adminVO 新增管理员参数
     * @return 管理员对象
     */
    @PutMapping
    public ResultMessage AddAdmin(AdminVO adminVO)
    {    AdminVO adminVO1=new AdminVO();
        adminVO1.setId(1);
        adminVO1.setAdminName("123");
         return ResultUtil.data(adminVO1);
    }

    /**
     * 获取管理员列表
     * @param page 第几页的管理员列表
     * @return 管理员列表
     */
    @GetMapping("{page}")
    public ResultMessage<AdminListVO> GetAdminList(@PathVariable String page)
    {      AdminListVO adminListVO=new AdminListVO();
           List<AdminVO> adminVOList=new ArrayList<>();
           adminVOList.add(new AdminVO(1,"111","111"));
           adminVOList.add(new AdminVO(2,"222","222"));
           adminVOList.add(new AdminVO(3,"333","333"));
           adminListVO.setAdminVOList(adminVOList);
          return ResultUtil.data(adminListVO);
    }

    /**
     *
     * @param id 管理员id
     * @return 删除成功
     */
    @DeleteMapping("{id}")
    public ResultMessage DelAdmin(@PathVariable String id)
    {
        return ResultUtil.success();
    }


}

