package cn.tml.innermost.admin.controller;

import cn.tml.innermost.admin.entity.vos.AdminRoleParam;
import cn.tml.innermost.admin.entity.vos.AdminRoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;

import cn.tml.innermost.admin.service.AdminRoleService;
import cn.tml.innermost.admin.entity.dos.AdminRole;

/**
* <p>
    *  前端控制器
    * </p>
*
* @author SHIJINTAO
* @since 2022-10-16
*/

    @RestController
@RequestMapping("listener/admin/admin-role")
        public class AdminRoleController {
    @Autowired
    private AdminRoleService adminRoleService;




    /**
     * 删除角色
     * @param adminRoleParam 角色对象
     * @return 删除成功
     */
    @DeleteMapping
    public ResultMessage DelRole(AdminRoleParam adminRoleParam)
    {
        return ResultUtil.success();
    }

    /**
     * 新增角色
     * @param adminRoleParam 角色对象
     * @return 角色对象
     */
    @PutMapping
    public ResultMessage AddRole(AdminRoleParam adminRoleParam)
    {
        AdminRoleVO adminRoleVO=new AdminRoleVO();
        adminRoleVO.setId(1);
        adminRoleVO.setAdminId(10);
        adminRoleVO.setRoleName("超级管理员");
        return ResultUtil.data(adminRoleVO);
    }

    /**
     * 修改角色
     * @param adminRoleParam 角色对象
     * @return 修改成功
     */
    @PostMapping
    public ResultMessage ChangeRole(AdminRoleParam adminRoleParam)
    {
        return ResultUtil.success();
    }

    /**
     * 获取角色
     * @param admin_id 管理员id
     * @return 角色对象
     */
    @GetMapping("{admin_id}")
    public ResultMessage GetRole(@PathVariable String admin_id)
    {
        AdminRoleVO adminRoleVO=new AdminRoleVO();
        adminRoleVO.setId(1);
        adminRoleVO.setAdminId(10);
        adminRoleVO.setRoleName("超级管理员");
        return ResultUtil.data(adminRoleVO);
    }
}

