package cn.tml.innermost.admin.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import org.redisson.misc.Hash;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@RestController
@RequestMapping("listener/admin/label")
public class LabelController {
    /**
     * 添加情绪标签
     * @param label 标签对象
     * @return 标签对象
     */
    @PutMapping
    public ResultMessage AddLabel(HashMap<Object,Object>label)
    {
        return ResultUtil.data(new Object());
    }

    /**
     * 修改标签
     * @param label 标签对象
     * @return 修改成功
     */
    @PostMapping
    public ResultMessage ChangeLabel(HashMap<Object,Object>label)
    {
        return ResultUtil.success();
    }


    /**
     * 获取所有情绪标签
     * @return 标签列表
     */
    @GetMapping
    public ResultMessage GetLabelList()
    {
        return ResultUtil.data(new Object());
    }

    /**
     * 删除情绪标签
     * @param id 标签id
     * @return 删除成功
     */
    @DeleteMapping("{id}")
    public ResultMessage DelLabel(@PathVariable String id)
    {
        return ResultUtil.success();
    }


}
