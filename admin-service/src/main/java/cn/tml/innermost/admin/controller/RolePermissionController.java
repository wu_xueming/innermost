package cn.tml.innermost.admin.controller;

import cn.tml.innermost.admin.entity.vos.RolePermissionParam;
import cn.tml.innermost.admin.entity.vos.RolePermissionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.elasticsearch.DataElasticsearchTest;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.admin.service.RolePermissionService;
import cn.tml.innermost.admin.entity.dos.RolePermission;

/**
* <p>
    *  前端控制器
    * </p>
*
* @author SHIJINTAO
* @since 2022-10-16
*/

    @RestController
@RequestMapping("listener/admin/role-permission")
        public class RolePermissionController {
    @Autowired
    private RolePermissionService rolePermissionService;


    @PostMapping(value = "listRolePermission")
    public ResultMessage listRolePermission() {
    return ResultUtil.success();
    }


    /**
     * 新增权限
     * @param rolePermissionParam 权限参数
     * @return 新增成功
     */
    @PutMapping
    public ResultMessage AddPermission(RolePermissionParam rolePermissionParam)
    {
        return ResultUtil.success();
    }

    /**
     * 删除权限
     * @param rolePermissionParam 权限参数
     * @return 删除成功
     */
    @DeleteMapping
    public ResultMessage DelPermission(RolePermissionParam rolePermissionParam)
    {
        return ResultUtil.success();
    }

    /**
     * 获取角角色权限
     * @param role_name 角色名
     * @return 角色权限
     */
    @GetMapping
    public ResultMessage GetRolePermission(String role_name)
    {   RolePermissionVO rolePermissionVO=new RolePermissionVO();
        rolePermissionVO.setId(10);
        rolePermissionVO.setRoleName("超级管理员");
        rolePermissionVO.setPermissionName("歌曲");
        rolePermissionVO.setPermissionDescription("对歌曲的增删改查");
        return ResultUtil.data(rolePermissionVO);
    }



}

