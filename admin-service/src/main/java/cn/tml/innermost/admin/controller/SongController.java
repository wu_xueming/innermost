package cn.tml.innermost.admin.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@RestController
@RequestMapping("listener/admin/song")
public class SongController {
    /**
     * 添加歌曲
     * @param song 歌曲对象
     * @return 歌曲对象
     */
    @PutMapping
    public ResultMessage AddSong(HashMap<Object,Object>song)
    {
        return ResultUtil.data(new Object());
    }

    /**
     * 删除歌曲
     * @param id 歌曲id
     * @return 删除成功
     */
    @DeleteMapping("{id}")
    public ResultMessage DelSong(@PathVariable String id)
    {
        return ResultUtil.success();
    }

    /**
     * 获取歌曲
     * @param id 歌曲id
     * @return 歌曲对象
     */
    @GetMapping("{id}")
    public ResultMessage GetSong(@PathVariable String id)
    {
        return ResultUtil.data(new Object());
    }

    /**
     * 修改歌曲信息
     * @param song 歌曲对象
     * @return 修改成功
     */
    @PostMapping
    public ResultMessage ChangeSong(HashMap<Object,Object> song)
    {
        return ResultUtil.success();
    }



}
