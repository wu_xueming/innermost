package cn.tml.innermost.admin.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@RestController
@RequestMapping("listener/admin/user")
public class UserController {

    /**
     * 删除用户
     * @param id 用户id
     * @return 删除成功
     */
    @DeleteMapping
    public ResultMessage DelUser(String id)
    {
        return ResultUtil.success();
    }


    /**
     * 修改用户
     * @param user 用户对象
     * @return 修改成功
     */
    @PostMapping
    public ResultMessage ChangeUser(HashMap<Object,Object> user)
    {
        return ResultUtil.success();
    }

    /**
     * 获取用户列表
     * @param page 用户列表的页数
     * @return 返回用户列表
     */
    @GetMapping("{page}")
    public ResultMessage GetUserList(@PathVariable String page)
    {
        return ResultUtil.data(new Object());
    }



}
