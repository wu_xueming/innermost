package cn.tml.innermost.admin.entity.vos;

import lombok.Data;

import java.util.List;

@Data
public class AdminListVO {
    private List<AdminVO> adminVOList;
}
