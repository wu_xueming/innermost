package cn.tml.innermost.admin.entity.vos;

import lombok.Data;

@Data
public class AdminRoleParam {
    /**
     * 管理员id
     */
    private Integer adminId;

    /**
     * 角色名
     */
    private String roleName;
}
