package cn.tml.innermost.admin.entity.vos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;
@Data
public class AdminRoleVO {
    /**
     * 			主键
     */
    private Integer id;

    /**
     * 管理员id
     */
    private Integer adminId;

    /**
     * 角色名
     */
    private String roleName;

}
