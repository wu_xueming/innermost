package cn.tml.innermost.admin.entity.vos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AdminVO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 管理员账号
     */
    private String adminName;

    /**
     * 管理员密码
     */
    private String adminPwd;

    public AdminVO() {
    }

    public AdminVO(Integer id, String adminName, String adminPwd) {
        this.id = id;
        this.adminName = adminName;
        this.adminPwd = adminPwd;
    }
}
