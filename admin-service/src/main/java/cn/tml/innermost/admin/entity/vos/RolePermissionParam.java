package cn.tml.innermost.admin.entity.vos;

public class RolePermissionParam {

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 权限名
     */
    private String permissionName;

    /**
     * 权限描述
     */
    private String permissionDescription;
}
