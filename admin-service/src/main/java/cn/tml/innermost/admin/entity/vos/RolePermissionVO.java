package cn.tml.innermost.admin.entity.vos;

import lombok.Data;

@Data
public class RolePermissionVO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 权限名
     */
    private String permissionName;

    /**
     * 权限描述
     */
    private String permissionDescription;
}
