package cn.tml.innermost.admin.mapper;

import cn.tml.innermost.admin.entity.dos.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
