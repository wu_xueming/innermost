package cn.tml.innermost.admin.service;

import cn.tml.innermost.admin.entity.dos.Admin;
import cn.tml.innermost.admin.entity.vos.AdminVO;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.security.token.Token;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
public interface AdminService extends IService<Admin> {

}
