package cn.tml.innermost.admin.service;

import cn.tml.innermost.admin.entity.dos.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
public interface RolePermissionService extends IService<RolePermission> {

}
