package cn.tml.innermost.admin.service.impl;

import cn.tml.innermost.admin.entity.dos.AdminRole;
import cn.tml.innermost.admin.mapper.AdminRoleMapper;
import cn.tml.innermost.admin.service.AdminRoleService;
import cn.tml.innermost.framework.properties.RestProperties;
import cn.tml.innermost.framework.resttemplate.RestTemplateConfig;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements AdminRoleService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RestProperties restProperties;

}
