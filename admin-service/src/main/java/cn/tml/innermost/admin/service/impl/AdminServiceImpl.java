package cn.tml.innermost.admin.service.impl;

import cn.tml.innermost.admin.entity.dos.Admin;
import cn.tml.innermost.admin.entity.vos.AdminVO;
import cn.tml.innermost.admin.mapper.AdminMapper;
import cn.tml.innermost.admin.service.AdminService;
import cn.tml.innermost.admin.utils.AdminTokenGenerate;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.framework.utils.UuidUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {







}
