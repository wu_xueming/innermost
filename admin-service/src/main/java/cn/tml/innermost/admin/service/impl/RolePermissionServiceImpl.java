package cn.tml.innermost.admin.service.impl;

import cn.tml.innermost.admin.entity.dos.RolePermission;
import cn.tml.innermost.admin.mapper.RolePermissionMapper;
import cn.tml.innermost.admin.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SHIJINTAO
 * @since 2022-10-16
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
