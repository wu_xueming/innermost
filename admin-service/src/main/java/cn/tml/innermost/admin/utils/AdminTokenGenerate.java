package cn.tml.innermost.admin.utils;


import cn.tml.innermost.admin.entity.dos.Admin;
import cn.tml.innermost.framework.security.AuthUser;
import cn.tml.innermost.framework.security.enums.UserEnums;
import cn.tml.innermost.framework.security.token.Token;
import cn.tml.innermost.framework.security.token.TokenUtil;
import cn.tml.innermost.framework.security.token.base.AbstractTokenGenerate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 管理员Token生成器
 */
@Component
public class AdminTokenGenerate extends AbstractTokenGenerate<Admin> {

    @Resource
    private TokenUtil tokenUtil;

    @Override
    public Token createToken(Admin admin, Boolean longTerm) {
        AuthUser authUser = new AuthUser(admin.getAdminName(), (long)admin.getId(), UserEnums.MANAGER, longTerm);
        return tokenUtil.createToken(authUser);
    }

    @Override
    public Token refreshToken(String refreshToken) {
        return tokenUtil.refreshToken(refreshToken);
    }
}
