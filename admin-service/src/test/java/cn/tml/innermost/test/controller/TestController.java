package cn.tml.innermost.test.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.security.AuthUser;
import cn.tml.innermost.framework.security.annotations.Login;
import cn.tml.innermost.framework.security.annotations.Logout;
import cn.tml.innermost.framework.security.context.UserContext;
import cn.tml.innermost.framework.security.enums.UserEnums;
import cn.tml.innermost.framework.security.enums.VerificationEnums;
import cn.tml.innermost.framework.security.token.Token;
import cn.tml.innermost.framework.security.token.TokenUtil;
import cn.tml.innermost.framework.utils.EmailUtil;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.framework.utils.SnowFlakeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("listener/admin/test")
public class TestController {
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private EmailUtil emailUtil;

    @PostMapping("login")
    public ResultMessage<Token> login(String name) {
        Token token = tokenUtil.createToken(new AuthUser(name, SnowFlakeUtil.getId(), UserEnums.USER, true));
        return ResultUtil.data(token);
    }

    @Logout
    @PostMapping("logout")
    public ResultMessage<Object> logout() {
        return ResultUtil.success();
    }

    @Login(value = true)
    @GetMapping("user")
    public ResultMessage<AuthUser> user() {
        AuthUser currentUser = UserContext.getCurrentUser();
        return ResultUtil.data(currentUser);
    }

    @GetMapping("code/{email}")
    public ResultMessage<Object> sendCode(@PathVariable String email) {
        emailUtil.sendCode(email, "邮箱测试", VerificationEnums.LOGIN);
        return ResultUtil.success();
    }
}
