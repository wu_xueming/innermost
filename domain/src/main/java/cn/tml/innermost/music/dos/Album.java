package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.AlbumParams;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_album")
@Data
public class Album implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 专辑id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Boolean deleteFlag;

    /**
     * 专辑名称
     */
    private String name;

    /**
     * 专辑发行时间
     */
    private String issueTime;

    /**
     * 专辑语种
     */
    private String languages;

    /**
     * 专辑曲风流派
     */
    private String genres;

    /**
     * 专辑发行公司
     */
    private String issueCompany;

    /**
     * 专辑包含的歌曲数量
     */
    private Integer musicNums;

    /**
     * 专辑播放量
     */
    private Integer playsNums;

    /**
     * 专辑收藏量
     */
    private Integer collectionNums;

    /**
     * 专辑封面图片
     */
    private String coverUrl;

    /**
     * 专辑描述
     */
    private String description;

    /**
     * 专辑类型
     */
    private String type;

    /**
     * qq音乐的id
     */
    private String albumMid;

    /**
     * qq音乐歌手id
     */
    private String singerMid;

    public static Album valueOf(AlbumParams albumParams){
        Album album = new Album();
        BeanUtils.copyProperties(albumParams,album);
        return album;
    }
}
