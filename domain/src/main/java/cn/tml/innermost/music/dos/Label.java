package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.LabelParams;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_label")
@Data
public class Label implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标签id
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Boolean deleteFlag;

    /**
     * 标签名称
     */
    private String name;

    /**
     * 分类
     */
    private String category;

    /**
     * 标签封面图
     */
    private String coverUrl;

    /**
     * 标签描述
     */
    private String description;

    /**
     * qq音乐唯一id
     */
    private String qqGroupId;

    public static Label valueOf(LabelParams labelParams) {
        Label label = new Label();
        BeanUtils.copyProperties(labelParams, label);
        return label;
    }
}
