package cn.tml.innermost.music.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_label_music")
@Data
public class LabelMusic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long labelId;

    /**
     * 歌曲id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicId;

    /**
     * 歌曲名
     */
    private String musicName;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 歌曲名
     */
    private String albumName;

    /**
     * 时长
     */
    private int duration;

    /**
     * 封面图
     */
    private String coverUrl;

    /**
     * 权重
     */
    private String weight;
}
