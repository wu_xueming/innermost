package cn.tml.innermost.music.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_label_PlayList")
@Data
public class LabelMusicList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标签id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long labelId;

    /**
     * 歌单id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long MusicListId;

    /**
     * 歌单标题
     */
    private String MusicListName;

    /**
     * 歌单作者
     */
    private String authorName;

    /**
     * 歌单播放量
     */
    private Integer playsNums;

    /**
     * 歌单收藏量
     */
    private Integer collectionNums;

    /**
     * 歌单封面图
     */
    private String coverUrl;

}
