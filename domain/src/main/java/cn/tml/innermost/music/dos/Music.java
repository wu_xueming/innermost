package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.MusicParams;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_music")
@Data
public class Music implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 歌曲id
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Boolean deleteFlag;

    /**
     * 歌曲名
     */
    private String name;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 音乐时长
     */
    private int duration;

    /**
     * 所属专辑
     */
    private String albumName;

    /**
     * 歌曲语种
     */
    private String languages;

    /**
     * 唱片公司
     */
    private String company;

    /**
     * 发现时间
     */
    private String pubTime;

    /**
     * 歌曲曲风流派
     */
    private String genres;

    /**
     * 歌曲播放量
     */
    private Integer playsNums;

    /**
     * 歌曲收藏量
     */
    private Integer collectionNums;

    /**
     * 歌曲封面图
     */
    private String coverUrl;

    /**
     * 歌曲音频路径
     */
    private String musicUrl;

    /**
     * 歌曲歌词
     */
    private String lyrics;

    /**
     * 歌曲描述
     */
    private String description;

    /**
     * qq音乐的id
     */
    private String albumMid;

    /**
     * qq音乐歌手id
     */
    private String singerMid;

    /**
     * qq音乐歌曲mid
     */
    private String musicMid;

    /**
     * qq音乐里歌曲评论量
     */
    private Integer qqCommentNums;

    /**
     * qq音乐里歌曲id
     */
    private Integer qqId;

    public static Music valueOf(MusicParams musicParams) {
        Music music = new Music();
        BeanUtils.copyProperties(musicParams,music);
        return music;
    }
}
