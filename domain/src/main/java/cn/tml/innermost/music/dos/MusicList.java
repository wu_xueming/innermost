package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.MusicListParams;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_playlist")
@Data
public class MusicList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 歌单id
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    /**
     * 修改者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Boolean deleteFlag;

    /**
     * 歌单标题
     */
    private String name;

    /**
     * 歌单作者
     */
    private String authorName;

    /**
     * 歌单歌曲总量
     */
    private Integer musicNums;

    /**
     * 歌单播放量
     */
    private Integer playsNums;

    /**
     * 歌单收藏量
     */
    private Integer collectionNums;

    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌单描述
     */
    private String description;

    /**
     * qq音乐唯一id
     */
    private String qqId;

    public static MusicList valueOf(MusicListParams musicListParams) {
        MusicList musicList = new MusicList();
        musicList.setName(musicListParams.getName());
        musicList.setId(musicListParams.getMusicListId());
        musicList.setAuthorName(musicListParams.getAuthorName());
        musicList.setCoverUrl(musicListParams.getCoverUrl());
        musicList.setDescription(musicListParams.getDescription());
        return musicList;
    }
}
