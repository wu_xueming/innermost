package cn.tml.innermost.music.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_playlist_music")
@Data
public class MusicListMusic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long MusicListId;

    /**
     * 歌曲id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicId;

    /**
     * 歌曲名
     */
    private String musicName;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 歌曲名
     */
    private String albumName;

    /**
     * 歌曲时长
     */
    private int duration;

    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌曲Url
     * @param MusicListMusicParams
     * @return
     */
    private String musicUrl;


}

