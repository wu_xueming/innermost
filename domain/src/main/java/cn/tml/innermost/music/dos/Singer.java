package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.SingerParams;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_singer")
@Data
public class Singer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 歌手id
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    /**
     * 更新者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 删除标记
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Boolean deleteFlag;

    /**
     * 歌手姓名
     */
    private String name;

    /**
     * 歌手地区
     */
    private String area;

    /**
     * 歌手曲风流派
     */
    private String genres;

    /**
     * 歌手性别（男，女，组合）
     */
    private Integer sex;

    /**
     * 歌手歌曲总量
     */
    private Integer musicNums;

    /**
     * 歌手专辑总量
     */
    private Integer albumNums;

    /**
     * 歌手粉丝数量
     */
    private Integer fansNums;

    /**
     * 歌手封面图
     */
    private String coverUrl;

    /**
     * 歌手描述
     */
    //@TableField(exist = false)
    private String description;



    public static Singer valueOf(SingerParams singerParams) {
        Singer singer = new Singer();
        BeanUtils.copyProperties(singerParams, singer);
        return singer;
    }
}
