package cn.tml.innermost.music.dos;

import cn.tml.innermost.music.params.AlbumSingerParams;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_singer_album")
@Data
public class SingerAlbum implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 歌手id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long singerId;

    /**
     * 专辑id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long albumId;

    /**
     * 专辑名
     */
    private String albumName;

    /**
     * 专辑封面
     */
    private String albumUrl;

    /**
     * 发现时间
     */
    private String issueTime;

    public static SingerAlbum valueOf(AlbumSingerParams albumSingerParams) {
        SingerAlbum singerAlbum = new SingerAlbum();
        BeanUtils.copyProperties(albumSingerParams, singerAlbum);
        return singerAlbum;
    }
}
