package cn.tml.innermost.music.dos;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@TableName("mt_singer_music")
@Data
@AllArgsConstructor
public class SingerMusic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 歌手id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long singerId;

    /**
     * 歌曲id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicId;

    /**
     * 歌曲名
     */
    private String musicName;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 音乐时长
     */
    private int duration;

    /**
     *  歌曲封面
     */
    private String coverUrl;

}
