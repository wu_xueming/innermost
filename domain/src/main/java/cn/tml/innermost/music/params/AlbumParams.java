package cn.tml.innermost.music.params;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AlbumParams {

    /**
     * 专辑名称
     */
    @NotBlank(message = "albumName Invalid")
    private String name;

    /**
     * 专辑发行时间
     */
    private String issueTime;

    /**
     * 专辑语种
     */
    private String languages;

    /**
     * 专辑曲风流派
     */
    private String genres;

    /**
     * 专辑发行公司
     */
    private String issueCompany;

    /**
     * 专辑封面图片
     */
    private String coverUrl;

    /**
     * 专辑描述
     */
    private String description;

    public AlbumParams() {
    }
}
