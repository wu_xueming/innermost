package cn.tml.innermost.music.params;
import cn.tml.innermost.music.dos.SingerAlbum;
import lombok.Data;

import java.util.List;

@Data
public class AlbumSingerParams {
    List<SingerAlbum> singerAlbumList;
}
