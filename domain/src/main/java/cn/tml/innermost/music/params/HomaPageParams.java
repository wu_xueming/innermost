package cn.tml.innermost.music.params;

import lombok.Data;

@Data
public class HomaPageParams {

    private int musicNums;

    private int albumNums;

    private int SingerNums;

    private int musicListNums;
}
