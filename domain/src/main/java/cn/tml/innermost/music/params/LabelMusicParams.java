package cn.tml.innermost.music.params;
import cn.tml.innermost.music.dos.LabelMusic;
import lombok.Data;

import java.util.List;
@Data
public class LabelMusicParams {
    List<LabelMusic> musicLabelList;
}
