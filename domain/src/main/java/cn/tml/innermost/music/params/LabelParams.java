package cn.tml.innermost.music.params;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class LabelParams {
    /**
     * 情绪标签or普通标签
     */
    private Boolean moodFlag;

    /**
     * 标签名称
     */

    @NotBlank(message = "labelName Invalid")
    private String name;
    /**
     * 标签封面图
     */
    private String coverUrl;

    /**
     * 标签描述
     */
    @NotNull(message = "description Invalid")
    private String description;

    public LabelParams() {
    }
}
