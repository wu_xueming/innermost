package cn.tml.innermost.music.params;

import cn.tml.innermost.music.dos.AlbumMusic;
import lombok.Data;

import java.util.List;

@Data
public class MusicAlbumParams {
    List<AlbumMusic> albums;
}
