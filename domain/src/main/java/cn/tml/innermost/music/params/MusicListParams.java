package cn.tml.innermost.music.params;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.sun.xml.internal.ws.developer.Serialization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Serialization
public class MusicListParams {

    /**
     * 歌单名称
     */
    @NotBlank(message = "MusicListName Invalid")
    private String name;

    /**
     * 歌单Id
     */
    @NotNull(message = "MusicListId Invalid")
    @JsonSerialize(using = ToStringSerializer.class)
    Long musicListId;

    /**
     * 歌单作者
     */

    private String authorName;

    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌单描述
     */
    private String description;

}
