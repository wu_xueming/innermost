package cn.tml.innermost.music.params;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class MusicParams {

    /**
     * 歌曲名
     */
    @NotBlank(message = "musicName Invalid")
    private String name;

    /**
     * 歌手名
     */
    @NotBlank(message = "singerName Invalid")
    private String singerName;

    /**
     * 音乐时长
     */
    private String duration;

    /**
     * 所属专辑
     */
    private String albumName;

    /**
     * 歌曲语种
     */

    private String languages;

    /**
     * 歌曲曲风流派
     */
    private String genres;

    /**
     * 歌曲播放量
     */
    private Integer playsNums;

    /**
     * 歌曲收藏量
     */
    private Integer collectionNums;

    /**
     * 歌曲封面图
     */
    private String coverUrl;

    /**
     * 歌曲音频路径
     */
    private String musicUrl;

    /**
     * 歌曲歌词
     */
    private String lyrics;

    /**
     * 歌曲描述
     */
    @NotNull(message = "description Invalid")
    private String description;

    public MusicParams() {
    }
}
