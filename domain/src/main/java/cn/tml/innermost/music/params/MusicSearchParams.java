package cn.tml.innermost.music.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicSearchParams {
    /**
        查询的关键字
    */
    String keyWords;

    /**
     查询的页码
     */
    Integer pageIndex;

    /**
     查询的返回的数量
     */
    Integer resultNums;


}
