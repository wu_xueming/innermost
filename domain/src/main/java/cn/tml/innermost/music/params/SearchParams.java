package cn.tml.innermost.music.params;

import lombok.Data;

@Data
public class SearchParams {

    private String content;

    private int current;

    private int size;

    private String searchField;
}
