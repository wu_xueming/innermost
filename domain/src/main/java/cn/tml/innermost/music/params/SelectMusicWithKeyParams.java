package cn.tml.innermost.music.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectMusicWithKeyParams {
    private String column;

    private String Value;

    private Integer nums;
}
