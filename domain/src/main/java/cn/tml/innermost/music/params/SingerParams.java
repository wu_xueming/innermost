package cn.tml.innermost.music.params;

import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class SingerParams {
    /**
     * 歌手姓名
     */
    @NotBlank(message = "singerName Invalid")
    private String name;

    /**
     * 歌手地区
     */
    private String area;

    /**
     * 歌手曲风流派
     */
    @NotNull
    private String genres;

    /**
     * 歌手性别（男，女，组合）
     */
    @NotNull(message = "singerSex Invalid")
    private Integer sex;

    /**
     * 歌手封面图
     */
 //  @URL
    private String coverUrl;

    /**
     * 歌手描述
     */
    @NotBlank(message = "description Invalid")
    private String description;

    public SingerParams() {
    }
}
