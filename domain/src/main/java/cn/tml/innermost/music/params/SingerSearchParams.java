package cn.tml.innermost.music.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SingerSearchParams {
    /**
     查询的关键字
     */
    String keyWords;

    /**
     查询的页码
     */
    Long pageIndex;

    /**
     查询的返回的数量
     */
    Long resultNums;


}
