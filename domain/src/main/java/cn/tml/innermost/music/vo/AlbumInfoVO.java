package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.Album;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class AlbumInfoVO {

    /**
     * 专辑id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 专辑名称
     */
    private String name;

    /**
     * 专辑发行时间
     */
    private String issueTime;

    /**
     * 专辑语种
     */
    private String languages;

    /**
     * 专辑曲风流派
     */
    private String genres;

    /**
     * 专辑发行公司
     */
    private String issueCompany;

    /**
     * 专辑包含的歌曲数量
     */
    private Integer musicNums;

    /**
     * 专辑播放量
     */
    private Integer playsNums;

    /**
     * 专辑收藏量
     */
    private Integer collectionNums;

    /**
     * 专辑封面图片
     */
    private String coverUrl;

    /**
     * 专辑描述
     */
    private String description;

    /**
     * 专辑类型
     */
    private String type;

    public static AlbumInfoVO valueOf(Album album) {
        AlbumInfoVO albumInfoVO = new AlbumInfoVO();
        BeanUtils.copyProperties(album, albumInfoVO);
        return albumInfoVO;
    }

}
