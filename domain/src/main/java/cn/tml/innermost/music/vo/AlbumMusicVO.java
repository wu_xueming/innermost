package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.AlbumMusic;
import lombok.Data;

import java.util.List;

@Data
public class AlbumMusicVO {

    private AlbumInfoVO albumInfoVO;

    private List<AlbumMusic> albumMusics;
}
