package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryList {

    private String categoryName;

    private List<LabelInfoVO> labelInfoVOList;
}
