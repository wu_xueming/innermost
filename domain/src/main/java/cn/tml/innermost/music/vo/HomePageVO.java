package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;


/**
 * @author 燧枫
 * @date 2023/5/5 13:28
*/
@Data
public class HomePageVO {

   List<MusicInfoVO> musicInfoVOList;

   List<AlbumInfoVO> albumInfoVOList;

   List<SingerInfoVO> singerInfoVOList;

   List<MusicListInfoVO> MusicListInfoVOList;

    public HomePageVO(List<MusicInfoVO> musicInfoVOList, List<AlbumInfoVO> albumInfoVOList, List<SingerInfoVO> singerInfoVOList, List<MusicListInfoVO> MusicListInfoVOList) {
        this.musicInfoVOList = musicInfoVOList;
        this.albumInfoVOList = albumInfoVOList;
        this.singerInfoVOList = singerInfoVOList;
        this.MusicListInfoVOList = MusicListInfoVOList;
    }

    public HomePageVO() {
    }
}
