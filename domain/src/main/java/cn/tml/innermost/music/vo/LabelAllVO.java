package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;


@Data
public class LabelAllVO {
    private List<CategoryList> categoryLists;
}

