package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.Label;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class LabelInfoVO {
    /**
     * 标签id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 标签名称
     */
    private String name;

    /**
     * 分类
     */
    private String category;

    /**
     * 标签封面图
     */
    private String coverUrl;

    /**
     * 标签描述
     */
    private String description;

    public static LabelInfoVO valueOf(Label label) {
        LabelInfoVO labelInfoVO = new LabelInfoVO();
        BeanUtils.copyProperties(label, labelInfoVO);
        return labelInfoVO;
    }
}
