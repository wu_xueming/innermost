package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.LabelMusicList;
import lombok.Data;

import java.util.List;

@Data
public class LabelMusicListVO {

    private PageOV pageOV;

    private LabelInfoVO labelInfoVO;

    private List<LabelMusicList> labelMusicLists;

    public LabelMusicListVO(PageOV pageOV, LabelInfoVO labelInfoVO, List<LabelMusicList> labelMusicLists) {
        this.pageOV = pageOV;
        this.labelInfoVO = labelInfoVO;
        this.labelMusicLists = labelMusicLists;
    }

    public LabelMusicListVO() {
    }
}
