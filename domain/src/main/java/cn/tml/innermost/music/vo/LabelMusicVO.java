package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.LabelMusic;
import lombok.Data;

import java.util.List;

@Data
public class LabelMusicVO {

    private PageOV pageOV;

    private LabelInfoVO labelInfoVO;

    private List<LabelMusic> musicLabelList;

    public LabelMusicVO(PageOV pageOV, LabelInfoVO labelInfoVO, List<LabelMusic> musicLabelList) {
        this.pageOV = pageOV;
        this.labelInfoVO = labelInfoVO;
        this.musicLabelList = musicLabelList;
    }

    public LabelMusicVO() {
    }
}
