package cn.tml.innermost.music.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicBaseInfoPageVO {

    private List<MusicBaseInfoVO> musicBaseInfoVOList;

    private PageOV pageOV;
}
