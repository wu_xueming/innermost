package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.Music;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicBaseInfoVO {
    /**
     * 歌曲id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 歌曲名
     */
    private String name;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 歌曲封面图
     */
    private String coverUrl;

    /**
     * 歌曲音频路径
     */
    private String musicUrl;

    private String duration;



    public static MusicBaseInfoVO valueOf(Music music) {
        MusicBaseInfoVO MusicBaseInfoVO = new MusicBaseInfoVO();
        BeanUtils.copyProperties(music, MusicBaseInfoVO);
        return MusicBaseInfoVO;
    }
}
