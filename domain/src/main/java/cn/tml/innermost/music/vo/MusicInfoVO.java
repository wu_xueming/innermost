package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.Music;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;


@Data
public class MusicInfoVO {

    /**
     * 歌曲id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 歌曲名
     */
    private String name;

    /**
     * 歌手名
     */
    private String singerName;

    /**
     * 歌曲时长
     */
    private int duration;

    /**
     * 所属专辑
     */
    private String albumName;

    /**
     * 歌曲语种
     */
    private String languages;

    /**
     * 唱片公司
     */
    private String company;

    /**
     * 发行时间
     */
    private String pubTime;

    /**
     * 歌曲曲风流派
     */
    private String genres;

    /**
     * 歌曲播放量
     */
    private Integer playsNums;

    /**
     * 歌曲收藏量
     */
    private Integer collectionNums;

    /**
     * 歌曲封面图
     */
    private String coverUrl;

    /**
     * 歌曲音频路径
     */
    private String musicUrl;

    /**
     * 歌曲描述
     */
    private String description;

    public static MusicInfoVO valueOf(Music music) {
        MusicInfoVO musicInfoVO = new MusicInfoVO();
        BeanUtils.copyProperties(music, musicInfoVO);
        return musicInfoVO;
    }
}
