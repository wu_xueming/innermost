package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.MusicList;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

@Data
public class MusicListInfoVO {

    /**
     * 歌单id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 歌单标题
     */
    private String name;

    /**
     * 歌单作者
     */
    private String authorName;

    /**
     * 歌单歌曲总量
     */
    private Integer musicNums;

    /**
     * 歌单播放量
     */
    private Integer playsNums;

    /**
     * 歌单收藏量
     */
    private Integer collectionNums;

    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌单描述
     */
    private String description;

    public static MusicListInfoVO valueOf(MusicList MusicList){
        MusicListInfoVO MusicListInfoVO = new MusicListInfoVO();
        BeanUtils.copyProperties(MusicList, MusicListInfoVO);
        return MusicListInfoVO;
    }
}
