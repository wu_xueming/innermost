package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.LabelMusicList;
import lombok.Data;

import java.util.List;

@Data
public class MusicListLabelVO {

    private PageOV pageOV;

    private LabelInfoVO labelInfoVO;

    private List<LabelMusicList> labelMusicListList;

    public MusicListLabelVO(PageOV pageOV, LabelInfoVO labelInfoVO, List<LabelMusicList> labelMusicListList) {
        this.pageOV = pageOV;
        this.labelInfoVO = labelInfoVO;
        this.labelMusicListList = labelMusicListList;
    }

    public MusicListLabelVO() {
    }
}
