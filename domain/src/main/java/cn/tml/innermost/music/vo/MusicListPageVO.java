package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;

@Data
public class MusicListPageVO {

    private PageOV pageOV;

    private List<MusicListInfoVO> MusicListInfoVOList;

    public MusicListPageVO(PageOV pageOV, List<MusicListInfoVO> MusicListInfoVOList) {
        this.pageOV = pageOV;
        this.MusicListInfoVOList = MusicListInfoVOList;
    }
}
