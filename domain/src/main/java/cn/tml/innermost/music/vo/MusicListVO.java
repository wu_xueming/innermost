package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.MusicListMusic;
import lombok.Data;

import java.util.List;

@Data
public class MusicListVO {

    private MusicListInfoVO MusicListInfoVO;

    private List<MusicListMusic> songList;

    public MusicListVO(MusicListInfoVO MusicListInfoVO, List<MusicListMusic> songList) {
        this.MusicListInfoVO = MusicListInfoVO;
        this.songList = songList;
    }
}
