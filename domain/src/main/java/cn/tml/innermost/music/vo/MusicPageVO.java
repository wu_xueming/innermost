package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;

@Data

public class MusicPageVO {

    private PageOV pageOV;

    private List<MusicInfoVO> musicInfoVOList;

    public MusicPageVO(PageOV pageOV, List<MusicInfoVO> musicInfoVOList) {
        this.pageOV = pageOV;
        this.musicInfoVOList = musicInfoVOList;
    }
}
