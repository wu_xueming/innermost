package cn.tml.innermost.music.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageOV {

    /**
     * 当前页码数
     */
    private Long current;

    /**
     * 每页记录数
     */
    private Long size;

    /**
     * 总记录条数
     */
    private Long total;

    /**
     * 总页码数
     */
    private Long pages;
}
