package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchVO {

    private PageOV pageOV;

    private List<?> objectList;
}
