package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.SingerAlbum;
import lombok.Data;

import java.util.List;

/**
 * 歌手的所有专辑
 */
@Data
public class SingerAlbumVO {

    private PageOV pageOV;

    SingerInfoVO singerInfoVO;

    List<SingerAlbum> singerAlbumList;

    public SingerAlbumVO(PageOV pageOV, SingerInfoVO singerInfoVO, List<SingerAlbum> singerAlbumList) {
        this.pageOV = pageOV;
        this.singerInfoVO = singerInfoVO;
        this.singerAlbumList = singerAlbumList;
    }
}
