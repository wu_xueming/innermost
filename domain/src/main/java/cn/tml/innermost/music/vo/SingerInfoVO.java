package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.Singer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * @description:
 * @author: asd
 * @time: 2022/10/21 16:56
 */
@Data
public class SingerInfoVO {

    /**
     * 歌手id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 歌手姓名
     */
    private String name;

    /**
     * 歌手地区
     */
    private String area;

    /**
     * 歌手曲风流派
     */
    private String genres;

    /**
     * 歌手性别（男，女，组合）
     */
    private Integer sex;

    /**
     * 歌手歌曲总量
     */
    private Integer musicNums;

    /**
     * 歌手专辑总量
     */
    private Integer albumNums;

    /**
     * 歌手粉丝数量
     */
    private Integer fansNums;

    /**
     * 歌手封面图
     */
    private String coverUrl;

    /**
     * 歌手描述
     */
    private String description;

    public static SingerInfoVO valueOf(Singer singer){
        SingerInfoVO singerInfoVO = new SingerInfoVO();
        BeanUtils.copyProperties(singer, singerInfoVO);
        return singerInfoVO;
    }
}
