package cn.tml.innermost.music.vo;

import cn.tml.innermost.music.dos.SingerMusic;
import lombok.Data;

import java.util.List;

/**
 * 歌手的所有歌曲
 */
@Data
public class SingerMusicVO {

    private PageOV pageOV;

    SingerInfoVO singerInfoVO;

    List<SingerMusic> singerMusicList;

    public SingerMusicVO(PageOV pageOV, SingerInfoVO singerInfoVO, List<SingerMusic> singerMusicList) {
        this.pageOV = pageOV;
        this.singerInfoVO = singerInfoVO;
        this.singerMusicList = singerMusicList;
    }
}
