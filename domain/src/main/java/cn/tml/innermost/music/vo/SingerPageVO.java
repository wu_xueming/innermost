package cn.tml.innermost.music.vo;

import lombok.Data;

import java.util.List;

@Data
public class SingerPageVO {

    private PageOV pageOV;

    private List<SingerInfoVO> singerInfoVOList;

    public SingerPageVO(PageOV pageOV, List<SingerInfoVO> singerInfoVOList) {
        this.pageOV = pageOV;
        this.singerInfoVOList = singerInfoVOList;
    }
}
