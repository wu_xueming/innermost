package cn.tml.innermost.recommend.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFeature {

    private Long userId;
    /**
     * 特征名字，值为
     */
    private String featureName;

    private String value;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
}
