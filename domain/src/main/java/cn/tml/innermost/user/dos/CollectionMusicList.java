package cn.tml.innermost.user.dos;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/21 16:44
 */
@Data
@TableName("mt_user_collection_playlist")
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class CollectionMusicList {

    @TableField("user_id")
    @NotNull(message = "用户id不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @TableField("music_list_id")
    @NotNull(message = "歌单id不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;

    private Integer musicCount;

    private String title;

    @NotBlank(message = "作者不能为空")
    private String authorName;

    private String coverUrl;


    public CollectionMusicList(long userId, Long MusicListId) {
        this.userId = userId;
        this.musicListId = MusicListId;
    }
}
