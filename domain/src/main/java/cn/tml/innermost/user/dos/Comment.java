package cn.tml.innermost.user.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

/**
 * @date 2023/4/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("comment")
public class Comment {
    //id
    @TableId(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //评论者id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    //音乐id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long  musicId;
    //内容
    private String content;
    //根评论id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rootId;
    //被评论的人的id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long toCommentUserId;
    //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
    //被评论歌单名
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;
    //删除标记
    private boolean del_flag;
}
