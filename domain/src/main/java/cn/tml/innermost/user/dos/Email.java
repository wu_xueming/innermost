package cn.tml.innermost.user.dos;

import lombok.Data;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/10/24 21:06
 */
@Data
public class Email {
    private String emailAddress;
    private String loginName;
    private String password;
    private Long id;
}