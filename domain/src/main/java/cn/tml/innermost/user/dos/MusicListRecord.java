package cn.tml.innermost.user.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("music_list_record")
public class MusicListRecord {
    @TableId
    private Long id;

    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;

    @NotNull
    private Long userId;

    @NotNull
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;

    @NotNull
    @TableField("music_list_name")
    private String name;

    @NotNull
    private String coverUrl;
}
