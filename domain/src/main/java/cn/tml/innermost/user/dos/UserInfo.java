package cn.tml.innermost.user.dos;


import cn.tml.innermost.framework.security.AuthUser;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

import java.time.LocalDateTime;

/**
 * @author welsir
 */
@Data
@TableName("mt_user")
public class UserInfo  {

    //用户唯一id
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    //用户账号
    @TableField("user_name")
    private String username;

    //用户密码
    private String password;

    //用户头像
    private String avatar;

    //用户生日
    private String birthday;

    //用户地区
    private String region;

    //性别
    private String sex;

    //默认歌单
    @TableField("default_play_list_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long defaultMusicListId;

    //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;

    //更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    //创建者
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    //更新者
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    //删除标记
    private Boolean deleteFlag = false;

    //邮箱地址
    private String emailAddress;

    //用户所选主题
    private String theme;

    //用户简介
    private String profile;

    //昵称
    private String nickName;


    public UserInfo(String username, String password, String emailAddress) {
        this.username = username;
        this.password = password;
        this.emailAddress = emailAddress;
    }

    public UserInfo(String password, String emailAddress) {
        this.password = password;
        this.emailAddress = emailAddress;
    }

    public UserInfo() {
    }

    public static boolean exist(UserInfo userInfo) {
        return userInfo != null;
    }

    public static UserInfo valueOf(AuthUser authUser) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(authUser, userInfo);
        return userInfo;
    }

}
