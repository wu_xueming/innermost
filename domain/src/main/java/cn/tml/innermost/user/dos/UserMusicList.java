package cn.tml.innermost.user.dos;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Date;
import java.time.LocalDateTime;

@TableName("mt_user_playlist")
@Data
@Builder
@AllArgsConstructor
public class UserMusicList {

    @TableField("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * @description: 用户对应的歌单id
     */
    @TableField("music_list_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;

    @TableField("title")
    private String name;

    @TableField("music_count")
    private Integer musicCount;

    @TableField("cover_url")
    private String coverUrl;

    @TableField("listen_count")
    private Integer playsNums;

    @TableField("author_name")
    private String authorName;

    @TableField("collection_count")
    private Integer collectionNums;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;

    @TableField("delete_flag")
    private Boolean deleteFlag;



    public UserMusicList(){}



}
