package cn.tml.innermost.user.vos.params;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/10/24 21:22
 */
@Data
@NoArgsConstructor
public class ChangePasswordParams {


    @NonNull
    @Length(min = 6,max = 15,message = "密码长度为6~15")
    private String password;

    @NonNull
    @Length(min = 5,max = 5,message = "验证码长度为5位")
    private String code;

    @NonNull
    @Length(min = 12,max = 17,message = "邮箱地址长度为12~17位")
    private String emailAddress;
}