package cn.tml.innermost.user.vos.params;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/11/12 19:50
 */
@Data
public class ChangeUserInfoParams {
    public ChangeUserInfoParams() {
    }

    @NonNull
    @Length(min = 1, max = 15, message = "账号长度为1~15位")
    private String userName;

    @Length(/*min = 1,*/max = 10, message = "昵称长度为1~10")
    private String nickName;

    @NonNull
    @Length(min = 6, max = 15, message = "密码长度为6~15")
    private String password;

    @Length(/*min = 10,*/max = 10, message = "日期长度为10 如（2022/11/11）")
    private String birthday;

    @Length(/*min = 6,*/max = 25, message = "地区长度为6~25")
    private String region;


    @Length(/*min = 1,*/max = 1, message = "性别长度为1")
    private String sex;

    private String avatar;

    @Length(/*min = 0,*/max = 1000, message = "自我介绍长度为1~1000")
    private String profile;
}