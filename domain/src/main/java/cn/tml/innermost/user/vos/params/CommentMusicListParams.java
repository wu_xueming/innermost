package cn.tml.innermost.user.vos.params;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @date 2023/4/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentMusicListParams {
    //音乐id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;

    //内容
    private String content;

    //根评论id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rootId;

    //被评论的人的id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long toCommentUserId;
}
