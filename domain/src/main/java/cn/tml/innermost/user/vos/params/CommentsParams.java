package cn.tml.innermost.user.vos.params;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
public class CommentsParams {

    @JsonSerialize(using = ToStringSerializer.class)
    private String musicId;

    private String text;

    @NonNull
    @Length(min = 11,max = 11,message = "手机号码长度为11位")
    @JsonSerialize(using = ToStringSerializer.class)
    private String userId;
}
