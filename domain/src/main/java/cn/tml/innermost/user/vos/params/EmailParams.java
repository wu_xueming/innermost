package cn.tml.innermost.user.vos.params;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/10/23 23:03
 */
@Data
public class EmailParams {
    public EmailParams() {
    }

    @NonNull
    @Length(min = 12,max = 17,message = "邮箱地址长度为12~17位")
    private String emailAddress;

    //验证码类型
    @NonNull
    @Length(min = 1,max = 1,message = "验证码类型为1位")
    private String type;


}