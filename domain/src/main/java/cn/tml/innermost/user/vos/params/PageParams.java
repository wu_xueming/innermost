package cn.tml.innermost.user.vos.params;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageParams {

    @Min(value = 1, message = "startPage must be greater than 0")
    private Integer startPage;

    @Min(value = 1, message = "startPage must be greater than 0")
    private Integer OnePageAmount;
}
