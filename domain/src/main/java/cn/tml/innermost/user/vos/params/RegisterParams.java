package cn.tml.innermost.user.vos.params;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/16 20:28
 */
@Data
public class RegisterParams {

    @NonNull
    @Length(min = 1, max = 15, message = "账号长度为1~15位")
    private String username;

    @NonNull
    @Length(min = 6, max = 15, message = "密码长度为6~15")
    private String password;


    @NonNull
    @Length(min = 12, max = 17, message = "邮箱长度为12~17位")
    private String emailAddress;
    @NonNull
    @Length(min = 5, max = 5, message = "验证码长度为5位")
    private String code;

    public RegisterParams() {
    }
}
