package cn.tml.innermost.user.vos.vo;

import cn.tml.innermost.user.dos.CollectionMusicList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/20 23:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CollectionMusicListVO {

    /**
     * 歌单id
     */
    private Long id;
    /**
     * 歌单标题
     */
    private String name;

    /**
     * 歌单作者
     */
    private String authorName;

    /**
     * 歌单歌曲总量
     */
    private Integer musicNums;
    /**
     * 歌单播放量
     */
    private Integer playsNums;

    /**
     * 歌单收藏量
     */
    private Integer collectionNums;

    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌单描述
     */
    private String description;

    public static CollectionMusicListVO valueOf(CollectionMusicList collectionMusicList){
        CollectionMusicListVO collectionMusicListVO = new CollectionMusicListVO();
        BeanUtils.copyProperties(collectionMusicList,collectionMusicListVO);
        return collectionMusicListVO;
    }
}
