package cn.tml.innermost.user.vos.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;

/**
 * @Date 2023/5/18
 * @Author xiaochun
 */
@Data
@AllArgsConstructor
public class IdVO {
    @JsonSerialize(using = ToStringSerializer.class)
    Long id;
}
