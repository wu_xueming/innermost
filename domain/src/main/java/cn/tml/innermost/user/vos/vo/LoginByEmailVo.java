package cn.tml.innermost.user.vos.vo;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/11/2 20:44
 */
@Data
public class LoginByEmailVo {
    public LoginByEmailVo() {
    }

    @NonNull
    @Length(min = 12,max = 17,message = "邮箱长度为12~17位")
    private String emailAddress;
    @NonNull
    @Length(min = 5,max = 5,message = "验证码长度为5位")
    private String code;
}