package cn.tml.innermost.user.vos.vo;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/16 21:26
 */
@Data
public class LoginByUserNameVO {
    @NonNull
    @Length(min = 1, max = 15, message = "账号长度为1~15位")
    private String username;

    @NonNull
    @Length(min = 6, max = 15, message = "密码长度为6~15")
    private String password;

    public LoginByUserNameVO() {
    }
}
