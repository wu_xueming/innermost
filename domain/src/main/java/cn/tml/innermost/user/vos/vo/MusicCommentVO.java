package cn.tml.innermost.user.vos.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2023/4/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicCommentVO {
    //id
    @TableId(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //评论者id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    //音乐id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long  musicId;
    //内容
    private String content;
    //被评论的人的id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long toCommentUserId;
    //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;

    private String nickName;

    private String avatar;
}
