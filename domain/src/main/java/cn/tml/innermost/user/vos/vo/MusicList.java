package cn.tml.innermost.user.vos.vo;

import lombok.Data;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/21 22:10
 */
@Data
public class MusicList {

    private String musicId;

    private String musicName;

    private String singerName;

//    private String album;

    private String timeLength;
}
