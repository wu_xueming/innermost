package cn.tml.innermost.user.vos.vo;

import cn.tml.innermost.user.dos.MusicListRecord;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MusicListRecordVO {

    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    private Long musicListId;

    @NotNull
    private Long userId;

    @NotNull
    private LocalDateTime createTime;

    @NotNull
    private String name;

    @NotNull
    private String coverUrl;

    public static MusicListRecordVO  valueOf(MusicListRecord record){
        MusicListRecordVO vo = new MusicListRecordVO();
        vo.setMusicListId(record.getMusicListId());
        vo.setUserId(record.getUserId());
        vo.setCoverUrl(record.getCoverUrl());
        vo.setCreateTime(record.getCreateTime());
        vo.setName(record.getName());
        return vo;
    }
}
