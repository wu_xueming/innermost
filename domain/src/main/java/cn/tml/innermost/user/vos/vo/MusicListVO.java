package cn.tml.innermost.user.vos.vo;

import cn.tml.innermost.user.dos.UserMusicList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import java.io.Serializable;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/23 18:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MusicListVO implements Serializable {


    /**
     * 歌单id
     */
    private Long id;

    /**
     * 歌单名称
     */
    private String name;

    /**
     * 歌单作者
     */
    private String authorName;

    /**
     * 歌单歌曲总量
     */
    private Integer musicNums;

    /**
     * 歌单播放量
     */
    private Integer playsNums;

    private Integer collectionNums;
    /**
     * 歌单封面图
     */
    private String coverUrl;

    /**
     * 歌单描述
     */
    private String description;

    public static MusicListVO valueOf(UserMusicList userMusicList) {
        MusicListVO MusicListVO = new MusicListVO();
        BeanUtils.copyProperties(userMusicList, MusicListVO);
        return MusicListVO;
    }



}
