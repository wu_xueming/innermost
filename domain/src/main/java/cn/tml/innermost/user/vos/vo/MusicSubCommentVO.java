package cn.tml.innermost.user.vos.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @date 2023/4/9
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicSubCommentVO {
    //id
    @TableId(value = "id")
    private Long id;
    //评论者id
    private Long userId;
    //歌单id
    private Long  musicListId;
    //内容
    private String content;
    //被评论的人的id
    private Long toCommentUserId;
    //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
}
