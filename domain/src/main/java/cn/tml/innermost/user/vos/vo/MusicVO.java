package cn.tml.innermost.user.vos.vo;

import lombok.Data;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/22 17:48
 */
@Data
public class MusicVO {

    private Long musicId;

    private String musicName;

    private String singerName;

    private String albumName;

}
