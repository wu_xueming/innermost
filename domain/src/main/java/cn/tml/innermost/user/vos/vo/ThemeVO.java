package cn.tml.innermost.user.vos.vo;

import lombok.Data;

@Data
public class ThemeVO {
    private String theme;
}
