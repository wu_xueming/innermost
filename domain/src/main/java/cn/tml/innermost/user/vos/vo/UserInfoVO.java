package cn.tml.innermost.user.vos.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author yuech
 * @version 1.0
 * @description: TODO
 * @date 2022/11/2 15:39
 */
@Data
public class UserInfoVO {

    //用户账号
    private String username;

    //昵称
    private String nickName;

    //用户头像
    private String avatar;

    //用户生日
    private String birthday;

    //用户地区
    private String region;

    //性别
    private String sex;

    //默认歌单
    @JsonSerialize(using = ToStringSerializer.class)
    private Long defaultMusicListId;

    //邮箱地址
    private String emailAddress;

    //用户简介
    private String profile;

}





