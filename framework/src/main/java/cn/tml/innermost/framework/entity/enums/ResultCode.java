package cn.tml.innermost.framework.entity.enums;

/**
 * 返回状态码
 */
public enum ResultCode {

    /**
     * 成功状态码
     */
    SUCCESS(200, "成功"),

    /**
     * 失败返回码
     */
    ERROR(400, "服务器繁忙，请稍后重试"),
    /**
     * 系统异常
     */
    LIMIT_ERROR(1001, "访问过于频繁，请稍后再试"),
    API_NOT_USE(1002,"接口未实现" ),
    API_Error(1003,"接口运行错误"),
    /**
     * 参数异常
     */
    PARAMS_ERROR(4001, "参数异常"),
    UUID_NOT_FIND(4002, "设备码获取失败"),

    /**
     * 用户
     */
    USER_EDIT_SUCCESS(20001, "用户修改成功"),
    USER_NOT_EXIST(20002, "用户不存在"),
    USER_NOT_LOGIN(20003, "用户未登录"),
    USER_AUTH_EXPIRED(20004, "用户已退出，请重新登录"),
    USER_AUTHORITY_ERROR(20005, "权限不足"),
    USER_NAME_EXIST(20006, "该账号已被注册"),
    USER_EMAIL_EXIST(20007, "该邮箱已被注册"),
    USER_EMAIL_NOT_EXIST(20008, "邮箱不存在"),
    USER_PASSWORD_ERROR(20009, "密码不正确"),
    USER_EDIT_ERROR(20010, "用户修改失败"),
    USER_OLD_PASSWORD_ERROR(20011, "旧密码不正确"),
    USER_COLLECTION_EXIST(20012, "无法重复收藏歌单"),
    OTHER_PLACE_LOGIN(20013, "账号在他处登录"),
    INSERT_PLAY_LIST_FAIL(20014,"新建歌单失败!"),
    INSERT_COLLECTION_PLAY_LIST_FAIL(20015,"收藏歌单失败!"),
    ADD_MUSIC_TO_MusicList_FAIL(20016,"歌曲添加失败!"),
    DELETE_MUSIC_MusicList_FAIL(20017,"删除歌单失败!"),
    GET_MusicList_FAIL(20018,"获取歌单信息失败!"),
    USER_USERNAME_ERROR(200019, "用户名不正确"),
    USER_LOGOFF(200020, "用户已注销"),
    USER_REGISTER_ERROR(20021, "用户注册失败"),
    TITLE_EXIST(20022,"歌单名称不能重复!"),
    RECORD_NOT_EXIST(20023, "播放历史不存在"),
    /**
     * OSS
     */
    OSS_EXCEPTION_ERROR(30001, "文件上传失败，请稍后重试"),
    IMAGE_FILE_EXT_ERROR(30002, "不支持图片格式"),
    FILE_TYPE_NOT_SUPPORT(30003, "不支持上传的文件类型！"),

    /**
     * 验证码
     */
    VERIFICATION_EMAIL_CHECKED_ERROR(40001, "邮箱验证码错误，请重新校验"),

    /**
     * 歌曲模块
     */
    MUSIC_GET_FAIL(50001, "获取歌曲失败"),
    MUSIC_SAVE_FAIL(50002, "新建歌曲失败"),
    MUSIC_UPDATE_FAIL(50003, "歌曲信息更新失败"),
    MUSIC_PLAYS_ADD_FAIL(50004, "增加播放量失败"),
    MUSIC_NOT_EXIST(50005, "歌曲不存在"),
    MUSIC_TOP10000_FAIL(50006, "top10000获取异常"),
    MUSIC_HOMEPAGE_PARAMS_FAIL(50007, "首页推荐数据分页参数错误"),

    /**
     * 专辑模块
     */
    ALBUM_GET_FAIL(60001, "获取专辑失败"),
    ALBUM_UPDATE_FAIL(60002, "专辑信息更新失败"),
    ALBUM_SAVE_FAIL(60003, "专辑添加失败"),
    ALBUM_TOP1000_FAIL(60004, "top1000获取异常"),

    /**
     * 歌单模块
     */
    MusicList_GET_FAIL(70001, "歌单信息获取失败"),
    MusicList_UPDATE_FAIL(70002, "歌单信息更新失败"),
    MusicList_SAVE_FAIL(70003, "新建歌单失败"),
    MusicList_ADD_FAIL(70004, "未全部添加成功"),
    MusicList_DEL_FAIL(70005, "未全部删除成功"),
    MusicList_SONG_EXIST(70006, "歌单中已存在该歌曲"),
    MusicList_TOP1000_FAIL(70007, "top1000获取异常"),
    /**
     * 歌手模块
     */

    SINGER_GET_FAIL(80001,"歌手信息获取失败" ),
    SINGER_SAVE_FAIL(80002,"歌手添加失败" ),
    SINGER_UPDATE_FAIL(80003,"歌手信息更新失败" ),
    SINGER_TOP100_FAIL(80007, "top100获取异常"),
    /**
     * 标签模块
     */
    LABEL_GET_FAIL(90001,"标签信息获取失败" ),
    LABEL_SAVE_FAIL(90002,"标签添加失败" ),
    LABEL_UPDATE_FAIL(90003,"标签信息更新失败" ),
    LABEL_DEL_FAIL(90004,"标签删除失败" ),

    /**
     * 搜索
     */
    SEARCH_CONTENT_EMPTY(110001,"搜索内容为空"),
    SEARCH_FAIL(110002,"当前参数不符合"),
    SEARCH_SIZE_TOO_BIG(110003,"搜索条数过大"),
    SEARCH_FAIL_FIELD(110004,"非法搜索字段"),


    /**
     * 其他
     */
    PAGE_TOO_BIG(1000001,"分页size过大，请缩小size值再请求一次" );

    private final Integer code;
    private final String message;


    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

}
