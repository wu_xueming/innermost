package cn.tml.innermost.framework.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 邮箱参数类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "wrx.email")
public class EmailProperties {
    private String user = "mi";
    /**
     * 邮箱服务器
     */
    private String smtpHost = "smtp.qq.com";
    /**
     * 邮箱端口
     */
    private String port = "587";
    /**
     * 邮箱协议
     */
    private String protocol = "smtp";
    private String smtpAuth = "true";
    /**
     * 邮箱账号
     */
    private String account = "";
    /**
     * 邮箱密码
     */
    private String psw = "";
}
