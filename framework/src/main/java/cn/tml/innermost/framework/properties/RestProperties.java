package cn.tml.innermost.framework.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * restTemplate服务拉取配置参数
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "wrx.rest")
public class RestProperties {
    private String userServiceUrl ="http://192.168.31.96:8888/listener/user/";
    private String adminServiceUrl ="http://47.99.78.191:8080/listener/admin/";
    private String musicServiceUrl ="http://192.168.31.205:8081/listener/music/";
    private String homepageServiceUrl ="http://192.168.31.238:8080/listener/homepage/";
}
