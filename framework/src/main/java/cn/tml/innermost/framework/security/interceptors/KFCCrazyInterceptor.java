package cn.tml.innermost.framework.security.interceptors;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

@Aspect
@Configuration
@Slf4j
public class KFCCrazyInterceptor {
    @Before("execution(public * cn.tml.innermost.*.controller.*.*(..))")
    public void interceptor() {
        LocalDateTime now = LocalDateTime.now();
        int day = now.getDayOfWeek().get(ChronoField.DAY_OF_WEEK);
        if (day == -1) {
            int i = RandomUtil.randomInt(1, 5);
            if (i == 3) {
                throw new IllegalArgumentException("KFC Crazy Thursday need $50");
            }
        }
    }
}
