package cn.tml.innermost.framework.security.interceptors;

import cn.tml.innermost.framework.cache.Cache;
import cn.tml.innermost.framework.entity.enums.ResultCode;
import cn.tml.innermost.framework.exception.ServiceException;
import cn.tml.innermost.framework.security.AuthUser;
import cn.tml.innermost.framework.security.annotations.Logout;
import cn.tml.innermost.framework.security.context.UserContext;
import cn.tml.innermost.framework.security.enums.UserEnums;
import cn.tml.innermost.framework.utils.RedisKeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Aspect
@Configuration
@Slf4j
public class LogoutInterceptor {
    @Autowired
    private Cache cache;

    @Before("@annotation(logout)")
    public void interceptor(Logout logout) {
        AuthUser authUser = UserContext.getCurrentUser();
        if (authUser == null) throw new ServiceException(ResultCode.USER_NOT_LOGIN);
        //判断用户身份类型
        UserEnums[] userEnums = logout.role();
        if (Arrays.stream(userEnums).noneMatch(role -> role == authUser.getRole()))
            throw new ServiceException(ResultCode.USER_NOT_LOGIN);
        String loginKey = RedisKeyUtil.loginKey(authUser);
        String uuid = (String) cache.get(loginKey);
        if (uuid == null) throw new ServiceException(ResultCode.USER_NOT_LOGIN);
        cache.remove(loginKey);
    }
}
