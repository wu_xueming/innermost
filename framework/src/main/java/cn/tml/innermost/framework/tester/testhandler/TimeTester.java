package cn.tml.innermost.framework.tester.testhandler;

import cn.tml.innermost.framework.tester.util.TestMasterUtil;

import java.util.function.Supplier;

public class TimeTester implements TestHandler {

    @Override
    public Object goTest(Supplier tester) {
        return TestMasterUtil.logTime(tester);
    }
}
