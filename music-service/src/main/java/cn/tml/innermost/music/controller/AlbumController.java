package cn.tml.innermost.music.controller;

import cn.tml.innermost.music.params.MusicAlbumParams;
import cn.tml.innermost.music.vo.AlbumMusicVO;
import cn.tml.innermost.music.vo.AlbumInfoVO;
import cn.tml.innermost.music.params.AlbumParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.music.service.AlbumService;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@RestController
@RequestMapping("listener/music/album")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    /***
     * 获取所有专辑信息
     * @param id
     * @return: AlbumInfoVO
     */
    @GetMapping(value = "albumInfo/{id}")
    public ResultMessage getAlbumInfo(@PathVariable Long id) {
        AlbumInfoVO albumInfo = albumService.getAlbumInfo(id);
        return ResultUtil.data(albumInfo);
    }

    /***
     * 获取专辑所有歌曲
     * @param id
     * @return: AlbumMusicVO
     */
    @GetMapping(value = "albumAllMusic/{id}")
    public ResultMessage getAlbumAllMusicById(@PathVariable Long id) {
        AlbumMusicVO albumMusicVO = albumService.getAlbumAllMusicById(id);
        return ResultUtil.data(albumMusicVO);
    }

    /***
     * 新建一个专辑
     * @param albumParams
     * @return: AlbumInfoVO
     */
    @PutMapping(value = "album")
    public ResultMessage addAlbum(@RequestBody @Validated AlbumParams albumParams) {
        AlbumInfoVO albumInfoVO = albumService.addAlbum(albumParams);
        return ResultUtil.data(albumInfoVO);
    }

    /**
     * 删除一个专辑
     * @param id
     * @return: AlbumInfoVO
     */
    @DeleteMapping(value = "album/{id}")
    public ResultMessage delAlbum(@PathVariable Long id) {
        AlbumInfoVO albumInfoVO = albumService.delAlbum(id);
        return ResultUtil.data(albumInfoVO);
    }

    /***
     * 修改专辑信息
     * @param id
     * @param albumParams
     * @return: AlbumInfoVO
     */
    @PostMapping(value = "album/{id}")
    public ResultMessage updateAlbum(@PathVariable Long id, @RequestBody @Validated AlbumParams albumParams) {
        AlbumInfoVO albumInfoVO = albumService.updateAlbum(id, albumParams);
        return ResultUtil.data(albumInfoVO);
    }

    /***.
     * 添加歌曲到专辑
     * @param musicAlbumParams
     * @return: AlbumInfoVO
     */
    @PutMapping (value = "musicToAlbum")
    public ResultMessage addMusicToAlbum(@RequestBody MusicAlbumParams musicAlbumParams) {
        AlbumInfoVO albumInfoVO = albumService.addMusicToAlbum(musicAlbumParams.getAlbums());
        return ResultUtil.data(albumInfoVO);
    }

    /***
     * 删除专辑中的歌曲
     * @param musicAlbumParams
     * @return: AlbumInfoVO
     */
    @DeleteMapping(value = "musicInAlbum")
    public ResultMessage delMusicInAlbum(@RequestBody MusicAlbumParams musicAlbumParams) {
        AlbumInfoVO albumInfoVO = albumService.delMusicToAlbum(musicAlbumParams.getAlbums());
        return ResultUtil.data(albumInfoVO);
    }
}

