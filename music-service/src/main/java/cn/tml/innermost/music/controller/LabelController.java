package cn.tml.innermost.music.controller;

import cn.tml.innermost.music.params.LabelMusicParams;
import cn.tml.innermost.music.params.LabelParams;
import cn.tml.innermost.music.vo.LabelAllVO;
import cn.tml.innermost.music.vo.LabelInfoVO;
import cn.tml.innermost.music.vo.LabelMusicVO;
import cn.tml.innermost.music.vo.LabelMusicListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.music.service.LabelService;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@RestController
@RequestMapping("listener/music/label")
public class LabelController {
    @Autowired
    private LabelService labelService;

    /***
     * @description: 获取所有标签信息
     * @return: LabelAllVO
     */
    @GetMapping(value = "allLabelInfo")
    public ResultMessage getAllLabelInfo() {
        LabelAllVO labelAllVO = labelService.getAllLabelInfo();
        return ResultUtil.data(labelAllVO);
    }

    /***
     * @description: 分页获取标签所有歌曲
     * @param id
     * @param current
     * @param size
     * @return: LabelMusicVO
     */
    @GetMapping(value = "labelAllMusic/{id}/{current}/{size}")
    public ResultMessage getLabelAllMusic(@PathVariable Long id, @PathVariable Long current, @PathVariable Long size) {
        LabelMusicVO labelMusicVO = labelService.getLabelAllMusic(id, current, size);
        return ResultUtil.data(labelMusicVO);
    }

    /***
     * @description: 分页获取标签所有歌曲
     * @param id
     * @param current
     * @param size
     * @return: LabelMusicVO
     */
    @GetMapping(value = "labelAllMusicList/{id}/{current}/{size}")
    public ResultMessage getLabelAllMusicList(@PathVariable Long id, @PathVariable Long current, @PathVariable Long size) {
        LabelMusicListVO labelMusicVO = labelService.getLabelAllMusicList(id, current, size);
        return ResultUtil.data(labelMusicVO);
    }


    /***
     * @description: 新建一个标签
     * @param labelParams
     */
    @PutMapping(value = "label")
    public ResultMessage addLabel(@RequestBody @Validated LabelParams labelParams) {
        LabelInfoVO labelInfoVO = labelService.addLabel(labelParams);
        return ResultUtil.data(labelInfoVO);
    }

    /**
     * @param id
     * @description: 删除一个标签
     */
    @DeleteMapping(value = "label/{id}")
    public ResultMessage delLabel(@PathVariable Long id) {
        LabelInfoVO labelInfoVO = labelService.delLabel(id);
        return ResultUtil.data(labelInfoVO);
    }

    /***
     * @description: 修改标签信息
     * @param id
     * @return:
     */
    @PostMapping(value = "label/{id}")
    public ResultMessage updateLabel(@PathVariable Long id,@RequestBody @Validated LabelParams labelParams) {
        LabelInfoVO labelInfoVO = labelService.updataLabel(id,labelParams);
        return ResultUtil.data(labelInfoVO);
    }

    /***
     * @description: 添加歌曲到标签
     * @param labelMusicParams
     * @return:
     */
    @PutMapping(value = "musicToLabel")
    public ResultMessage addMusicToLabel(@RequestBody LabelMusicParams labelMusicParams) {
        LabelInfoVO labelInfoVO = labelService.addMusicToLabel(labelMusicParams.getMusicLabelList());
        return ResultUtil.data(labelInfoVO);
    }

    /***
     * @description: 删除标签中的歌曲
     * @param labelMusicParams
     */
    @DeleteMapping(value = "musicInLabel")
    public ResultMessage delMusicInLabel(@RequestBody LabelMusicParams labelMusicParams) {
        LabelInfoVO labelInfoVO = labelService.delMusicToLabel(labelMusicParams.getMusicLabelList());
        return ResultUtil.data(labelInfoVO);
    }
}

