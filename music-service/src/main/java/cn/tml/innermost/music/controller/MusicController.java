package cn.tml.innermost.music.controller;

import cn.tml.innermost.music.params.HomaPageParams;
import cn.tml.innermost.music.params.MusicParams;
import cn.tml.innermost.music.params.MusicSearchParams;
import cn.tml.innermost.music.params.SelectMusicWithKeyParams;
import cn.tml.innermost.music.vo.HomePageVO;
import cn.tml.innermost.music.vo.MusicBaseInfoPageVO;
import cn.tml.innermost.music.vo.MusicInfoVO;
import cn.tml.innermost.music.vo.MusicPageVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.music.service.MusicService;

import javax.annotation.Resource;
import java.nio.file.Path;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@RestController
@RequestMapping("listener/music")

public class MusicController {

    @Resource
    private MusicService musicService;

    /**
     * 首页推荐
     * @return
     */
    @PostMapping(value = "/homePage")
    public ResultMessage getHomePage(@RequestBody HomaPageParams homaPageParams) {
        HomePageVO musicInfoVO = musicService.getHomePage(homaPageParams);
        return ResultUtil.data(musicInfoVO);
    }

    @PostMapping("/search")
    public ResultMessage search(@RequestBody MusicSearchParams musicSearchParams){
        List<MusicInfoVO> searchResult = musicService.search(musicSearchParams);
        return ResultUtil.data(searchResult);
    }

    /***
     * @description: 根据id获取歌曲音频链接
     * @param "歌曲id"
     * @return: MusicInfoVO
     */
    @GetMapping(value = "/musicUrl/{id}")
    public ResultMessage getMusicUrl(@PathVariable Long id) {
        MusicInfoVO musicInfoVO = musicService.getMusicUrl(id);
        return ResultUtil.data(musicInfoVO);
    }

    /***
     * @description: 根据id获取歌曲
     * @param "歌曲id"
     * @return: MusicInfoVO
     */
    @GetMapping(value = "/music/{id}")
    public ResultMessage getMusic(@PathVariable Long id) {
        MusicInfoVO musicInfoVO = musicService.getMusic(id);
        return ResultUtil.data(musicInfoVO);
    }

    /**
     * 根据id获取歌曲歌词
     * @param musicId
     * @return
     */
    @GetMapping("/lyrics/{musicId}")
    public ResultMessage getLyrics(@PathVariable Long musicId){
        String lyrics = musicService.getMusicLyrics(musicId);
        return ResultUtil.data(lyrics);
    }

    /**
     * 批量获取歌曲
     * @param musicIdList
     * @return
     */
    @PostMapping("/list")
    public ResultMessage<List<MusicInfoVO>> selectMusicList(@RequestBody List<Long> musicIdList){
        List<MusicInfoVO> result = musicService.selectMusicList(musicIdList);
        return ResultUtil.data(result);
    }

    @PostMapping("/listWithColumn")
    public ResultMessage<List<MusicInfoVO>> selectMusicListWithColumn(SelectMusicWithKeyParams params){
        List<MusicInfoVO> musicInfoVOS = musicService.selectMusicListWithColumn(params);
        return ResultUtil.data(musicInfoVOS);
    }

    /***
     * @description: 分页获取所有歌曲
     * @param current
     * @param size
     * @return: MusicPageVO
     */
    @GetMapping(value = "/allMusic/{current}/{size}")
    public ResultMessage getAllMusic(@PathVariable Long current, @PathVariable Long size) {
        MusicPageVO musicPageVO = musicService.getAllMusic(current, size);
        return ResultUtil.data(musicPageVO);
    }

    /***
     * 增加歌曲的播放量
     * @param musicId
     * @return: OK
     */
    @PutMapping(value = "/musicPlays/{musicId}")
    public ResultMessage addMusicPlayNums(@PathVariable Long musicId) {
        musicService.addMusicPlayNums(musicId);
        return ResultUtil.success();
    }

    /***
     * @description: 新建一首歌曲
     * @param musicParams
     * @return: MusicInfoVO
     */
    @PutMapping(value = "music")
    public ResultMessage addMusic(@RequestBody @Validated MusicParams musicParams) {
        MusicInfoVO musicInfoVO = musicService.addMusic(musicParams);
        return ResultUtil.data(musicInfoVO);
    }

    /**
     * @param id
     * @description: 删除一首歌曲
     * @return: MusicInfoVO
     */
    @DeleteMapping(value = "music/{id}")
    public ResultMessage delLMusic(@PathVariable Long id) {
        MusicInfoVO musicInfoVO = musicService.delMusic(id);
        return ResultUtil.data(musicInfoVO);
    }

    /***
     * @description: 修改歌曲信息
     * @param id
     * @param musicParams
     * @return: MusicInfoVO
     */
    @PostMapping(value = "{id}")
    public ResultMessage updateMusic(@PathVariable Long id, @RequestBody @Validated MusicParams musicParams) {
        MusicInfoVO musicInfoVO = musicService.updateMusic(id, musicParams);
        return ResultUtil.data(musicInfoVO);
    }

    @PostMapping("incrListenCount/{musicId}")
    public ResultMessage incrListenCount(@PathVariable Long musicId,int count){
        MusicInfoVO musicInfoVO = musicService.incrMusicListenCount(musicId, count);
        return ResultUtil.data(musicInfoVO);
    }
}

