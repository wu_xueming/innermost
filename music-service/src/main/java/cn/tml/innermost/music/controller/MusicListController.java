package cn.tml.innermost.music.controller;

import cn.tml.innermost.music.params.MusicSearchParams;
import cn.tml.innermost.music.service.MusicListService;
import cn.tml.innermost.music.vo.MusicListVO;
import cn.tml.innermost.music.vo.MusicListInfoVO;
import cn.tml.innermost.music.params.MusicListParams;
import cn.tml.innermost.music.vo.MusicListPageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@RestController
@RequestMapping("listener/music/musicList")
@Slf4j
public class MusicListController {

    @Resource
    private MusicListService musicListService;

//    /***
//     * @description: 获取首页歌单信息
//     * @param size
//     * @return: HomePageVO
//     */
//    @GetMapping(value = "getHomepageList/{size}")
//    public ResultMessage getHomepagelist(@PathVariable Long size) {
//        HomePageVO MusicListInfoVO = musicListService.getHomepagelist(size);
//        return ResultUtil.data(MusicListInfoVO);
//    }


    /***
     * @description: 新建歌单
     * @param musicListParams
     * @return: MusicListInfoVO
     */
    @PutMapping(value = "")
    public ResultMessage addMusicList(@RequestBody @Validated MusicListParams musicListParams) {
        log.info(String.valueOf(musicListParams));
        MusicListInfoVO MusicListInfoVO = musicListService.addMusicList(musicListParams);
        return ResultUtil.data(MusicListInfoVO);
    }

    /***
     * @description: 删除歌单
     * @param id
     * @return: MusicListInfoVO
     */
    @DeleteMapping(value = "{id}")
    public ResultMessage delMusicList(@PathVariable Long id) {
        musicListService.delMusicList(id);
        return ResultUtil.success();
    }

    /***
     * @description: 修改歌单信息
     * @param musicListParams
     * @return: MusicListInfoVO
     */
    @PostMapping(value = "")
    public ResultMessage updateMusicList(@RequestBody MusicListParams musicListParams) {
        MusicListInfoVO res = musicListService.updateMusicList(musicListParams);
        return ResultUtil.data(res);
    }

    @PostMapping("/search")
    public ResultMessage searchMusicList(@RequestBody MusicSearchParams musicSearchParams){
        List<MusicListInfoVO> musicListInfoVOS = musicListService.selectMusicList(musicSearchParams);
        return ResultUtil.data(musicListInfoVOS);
    }

    /***
     * @description: 获取歌单信息
     * @param id 歌单id
     * @return: MusicListInfoVO
     */
    @GetMapping(value = "MusicListInfo/{id}")
    public ResultMessage getMusicListInfo(@PathVariable Long id) {
        MusicListInfoVO musicListInfoVO = musicListService.getMusicListInfo(id);
        return ResultUtil.data(musicListInfoVO);
    }


    /***
     * @description: 分页获取所有歌单信息
     * @param current
     * @param size
     * @return: MusicListPageVO
     */
    @GetMapping(value = "allMusicListInfo/{current}/{size}")
    public ResultMessage getAllMusicListInfo(@PathVariable Long current, @PathVariable Long size) {
        MusicListPageVO MusicListPageVO = musicListService.getAllMusicListInfo(current, size);
        return ResultUtil.data(MusicListPageVO);
    }

    /***
     * @description: 获取歌单所有歌曲
     * @param id
     * @return: MusicListVO
     */
    @GetMapping(value = "musicListAllMusic/{id}")
    public ResultMessage getMusicListAllMusic(@PathVariable Long id) {
        MusicListVO musicListVO = musicListService.getMusicListAllMusic(id);
        return ResultUtil.data(musicListVO);
    }

    /***
     * @description: 给歌单添加歌曲
     * @param musicListParams
     * @return: MusicListInfoVO
     */
    @PutMapping(value = "songToMusicList/{musicId}/{musicListId}")
    public ResultMessage addMusicToMusicList(@Validated @PathVariable Long musicId,
                                             @Validated @PathVariable Long musicListId) {
        MusicListInfoVO MusicListInfoVO = musicListService.addMusicToMusicList(musicId, musicListId);
        return ResultUtil.data(MusicListInfoVO);
    }

    /***
     * @description: 删除歌单中的歌曲
     * @param musicListParams
     * @return: MusicListInfoVO
     */
    @DeleteMapping(value = "delMusicFromList/{musicId}/{MusicListId}")
    public ResultMessage delMusicInMusicList(@Validated @PathVariable Long musicId, @PathVariable Long MusicListId) {
        MusicListInfoVO MusicListInfoVO = musicListService.delMusicToMusicList(musicId, MusicListId);
        return ResultUtil.data(MusicListInfoVO);
    }

    /***
     * @description: 歌单添加播放量
     * @param id
     * @param count
     * @return: MusicListInfoVO
     */
    @PutMapping("addMusicListListenCount/{id}/{count}")
    public ResultMessage addMusicListListenCount(@PathVariable Long id, @PathVariable int count) {
        Boolean res = musicListService.addListenCount(id, count);
        return ResultUtil.data(res);
    }
}
