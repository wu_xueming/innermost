package cn.tml.innermost.music.controller;

import cn.tml.innermost.music.dos.SingerMusic;
import cn.tml.innermost.music.params.*;
import cn.tml.innermost.music.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.music.service.SingerService;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@RestController
@RequestMapping("listener/music/singer")
public class SingerController {
    @Autowired
    private SingerService singerService;

    @PostMapping("/search")
    public ResultMessage searchSinger(@RequestBody MusicSearchParams musicSearchParams){
        List<SingerInfoVO> result = singerService.search(musicSearchParams);
        return ResultUtil.data(result);
    }

    /***
     * @description: 获取歌手信息
     * @param id
     * @return: SingerInfoVO
     */
    @GetMapping(value = "singerInfo/{id}")
    public ResultMessage getSingerInfo(@PathVariable Long id) {
        SingerInfoVO singerInfoVO = singerService.getSingerInfo(id);
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 分页获取所有歌手
     * @param  current
     * @param  size
     * @return: SingerPageVO
     */
    @GetMapping(value = "allSingerInfo/{current}/{size}")
    public ResultMessage getAllSingerInfo(@PathVariable Long current, @PathVariable Long size) {
        SingerPageVO singerPageVO = singerService.getAllSingerInfo(current, size);
        return ResultUtil.data(singerPageVO);
    }

    // 快速建立歌手与歌曲的关系表
    @GetMapping(value = "creatSingerAndMusic/{current}/{size}")
    public ResultMessage creatSingerAndMusic(@PathVariable Long current, @PathVariable Long size) {
        singerService.creatSingerAndMusic(current, size);
        return ResultUtil.success();
    }

    /***
     * @description: 获取歌手所有歌曲
     * @param id
     * @return: MusicSingerVO
     */
    @GetMapping(value = "singerAllMusic/{id}/{current}/{size}")
    public ResultMessage getSingerAllMusic(@PathVariable Long id, @PathVariable Long current, @PathVariable Long size) {
        SingerMusicVO singerMusicVO = singerService.getSingerAllMusic(id, current, size);
        return ResultUtil.data(singerMusicVO);
    }

    /***
     * @description: 获取歌手所有专辑
     * @param id
     * @return:
     */
    @GetMapping(value = "singerAllAlbum/{id}/{current}/{size}")
    public ResultMessage getSingerAllAlbum(@PathVariable Long id, @PathVariable Long current, @PathVariable Long size) {
        SingerAlbumVO singerAlbumVO = singerService.getSingerAllAlbum(id, current, size);
        return ResultUtil.data(singerAlbumVO);
    }

    /***
     * @description: 新建一个歌手
     * @param singerParams
     * @return:
     */
    @PutMapping(value = "singer")
    public ResultMessage addSinger(@RequestBody @Validated SingerParams singerParams) {
        SingerInfoVO singerInfoVO = singerService.addSinger(singerParams);
        return ResultUtil.data(singerInfoVO);
    }

    /**
     * @param singerId
     * @description: 删除一个歌手
     * @return:
     */
    @DeleteMapping(value = "singer/{id}")
    public ResultMessage delSinger(@PathVariable("id") Long singerId) {
        SingerInfoVO singerInfoVO = singerService.delSinger(singerId);
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 修改歌手信息
     * @param singerId
     * @param singerParams
     * @return:
     */
    @PostMapping(value = "singer/{id}")
    public ResultMessage updateSinger(@PathVariable(value = "id") Long singerId,@RequestBody @Validated SingerParams singerParams) {
        SingerInfoVO singerInfoVO = singerService.updataSinger(singerId,singerParams);
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 添加歌曲到歌手
     * @param singerMusicList
     * @return:SingerInfoVO
     */
    @PutMapping(value = "musicToSinger")
    public ResultMessage addMusicToSinger(@RequestBody List<SingerMusic> singerMusicList) {
        SingerInfoVO singerInfoVO = singerService.addMusicToSinger(singerMusicList);
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 删除歌手中的歌曲
     * @param singerMusicList
     * @return: SingerInfoVO
     */
    @DeleteMapping(value = "musicInSinger")
    public ResultMessage delMusicInSinger(@RequestBody List<SingerMusic> singerMusicList) {
        SingerInfoVO singerInfoVO = singerService.delMusicToSinger(singerMusicList);
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 添加专辑到歌手
     * @param albumSingerParams
     * @return:
     */
    @PutMapping(value = "albumToSinger")
    public ResultMessage addAlbumToSinger(@RequestBody AlbumSingerParams albumSingerParams) {
        SingerInfoVO singerInfoVO = singerService.addAlbumToSinger(albumSingerParams.getSingerAlbumList());
        return ResultUtil.data(singerInfoVO);
    }

    /***
     * @description: 删除歌手中的专辑
     * @param albumSingerParams
     * @return:
     */
    @DeleteMapping(value = "albumInSinger")
    public ResultMessage delAlbumInSinger(@RequestBody AlbumSingerParams albumSingerParams) {
        SingerInfoVO singerInfoVO = singerService.delAlbumToSinger(albumSingerParams.getSingerAlbumList());
        return ResultUtil.data(singerInfoVO);
    }

}

