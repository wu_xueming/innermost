/**
  * Copyright 2023 json.cn 
  */
package cn.tml.innermost.music.entity.pachong;

import lombok.Data;

@Data
public class Comm {
    private Long cv = 4747474L;
    private int ct = 24;
    private String format = "json";
    private String inCharset = "utf-8";
    private String outCharset = "utf-8";
    private int notice = 0;
    private String platform = "yqq.json";
    private int needNewCode = 1;
    private Long uin = 3250162035L;
    private Long g_tk_new_20200303 = 442016146L;
    private Long g_tk = 442016146L;
}