/**
  * Copyright 2023 json.cn 
  */
package cn.tml.innermost.music.entity.pachong;

import lombok.Data;

@Data
public class JsonRootBean {
    private Comm comm = new Comm();
    private MusicURL req_1;
}