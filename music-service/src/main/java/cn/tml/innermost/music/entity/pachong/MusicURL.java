package cn.tml.innermost.music.entity.pachong;

import lombok.Data;

@Data
public class MusicURL {

    private String module = "vkey.GetVkeyServer";
    private String method = "CgiGetVkey";
    private MusicURLParam param;

    public MusicURL(String[] mids) {
        param = new MusicURLParam(mids);
    }
}

@Data
class MusicURLParam {

    String guid = "4253368925";

    String[] songmid;

    int[] songtype;

    String uin = "3250162035";

    int loginflag = 1;

    String platform = "20";

    public MusicURLParam(String[] mids) {
        songmid = new String[mids.length];
        songtype = new int[mids.length];
        for (int i = 0; i < mids.length; i++) {
            songmid[i] = mids[i];
            songtype[i] = 0;
        }
    }
}

