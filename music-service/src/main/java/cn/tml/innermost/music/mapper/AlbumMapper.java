package cn.tml.innermost.music.mapper;

import cn.tml.innermost.music.dos.Album;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface AlbumMapper extends BaseMapper<Album> {

    List<Album> getTop1000ByIssueTime();
}
