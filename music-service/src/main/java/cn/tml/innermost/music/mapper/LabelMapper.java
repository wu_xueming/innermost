package cn.tml.innermost.music.mapper;

import cn.tml.innermost.music.dos.Label;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@Mapper
public interface LabelMapper extends BaseMapper<Label> {


}
