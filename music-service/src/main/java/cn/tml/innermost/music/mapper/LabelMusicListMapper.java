package cn.tml.innermost.music.mapper;

import cn.tml.innermost.music.dos.LabelMusicList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface LabelMusicListMapper extends BaseMapper<LabelMusicList> {

}
