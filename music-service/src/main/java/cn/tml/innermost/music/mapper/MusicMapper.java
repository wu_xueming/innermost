package cn.tml.innermost.music.mapper;

import cn.tml.innermost.music.dos.Music;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
@Mapper
public interface MusicMapper extends BaseMapper<Music> {

    List<Music> getTop10000();
}
