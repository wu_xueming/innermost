package cn.tml.innermost.music.mapper;

import cn.tml.innermost.music.dos.Music;
import cn.tml.innermost.music.dos.Singer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface SingerMapper extends BaseMapper<Singer> {

    List<Singer> getTop100();
}
