package cn.tml.innermost.music.service;

import cn.tml.innermost.music.dos.Album;
import cn.tml.innermost.music.dos.AlbumMusic;
import cn.tml.innermost.music.vo.AlbumMusicVO;
import cn.tml.innermost.music.vo.AlbumInfoVO;
import cn.tml.innermost.music.params.AlbumParams;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface AlbumService extends IService<Album> {

    /**
     * 获取专辑信息
     *
     * @param albumId
     * @return AlbumInfoVO
     */
    AlbumInfoVO getAlbumInfo(Long albumId);

    /**
     * 获取专辑所有歌曲
     *
     * @param albumId
     * @return AlbumMusicVO
     */
    AlbumMusicVO getAlbumAllMusicById(Long albumId);

    /***
     * 新建一个专辑
     * @param albumParams
     * @return: AlbumInfoVO
     */
    AlbumInfoVO addAlbum(AlbumParams albumParams);

    /***
     * 删除一个专辑
     * @param albumId
     * @return: AlbumInfoVO
     */
    AlbumInfoVO delAlbum(Long albumId);

    /***
     * 修改专辑信息
     * @param albumId
     * @param MusicListParams
     * @return: MusicListInfoVO
     */
    AlbumInfoVO updateAlbum(Long albumId, AlbumParams albumParams);

    /***
     * 添加歌曲到专辑
     * @param albumMusics
     * @return: AlbumInfoVO
     */
    AlbumInfoVO addMusicToAlbum(List<AlbumMusic> albumMusics);

    /***
     * 删除专辑中的歌曲
     * @param albumMusics
     * @return: AlbumInfoVO
     */
    AlbumInfoVO delMusicToAlbum(List<AlbumMusic> albumMusics);

    List<AlbumInfoVO> getRandomAlbumsFromTop1000(int count);
}
