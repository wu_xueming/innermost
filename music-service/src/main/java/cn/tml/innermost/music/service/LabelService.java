package cn.tml.innermost.music.service;

import cn.tml.innermost.music.dos.Label;
import cn.tml.innermost.music.dos.LabelMusic;
import cn.tml.innermost.music.params.LabelParams;
import cn.tml.innermost.music.vo.LabelInfoVO;
import cn.tml.innermost.music.vo.LabelAllVO;
import cn.tml.innermost.music.vo.LabelMusicVO;
import cn.tml.innermost.music.vo.LabelMusicListVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface LabelService extends IService<Label> {

    /***
     * 分页获取所有标签信息
     * @param 当前页码 current
     * @param 每页条数 size
     * @return: LabelAllVO
     */
    LabelAllVO getAllLabelInfo();

    /***
     * 分页获取标签所有歌曲
     * @param id
     * @param 当前页码 current
     * @param 每页条数 size
     * @return: LabelMusicVO
     */
    LabelMusicVO getLabelAllMusic(Long id, Long current, Long size);

    /***
     * 分页获取标签所有歌单
     * @param id
     * @param 当前页码 current
     * @param 每页条数 size
     * @return: LabelMusicVO
     */
    LabelMusicListVO getLabelAllMusicList(Long id, Long current, Long size);

    /***
     *  新建一个标签
     * @param labelParams
     */
    LabelInfoVO addLabel(LabelParams labelParams);

    /**
     * @param id
     * @description: 删除一个标签
     */

    LabelInfoVO delLabel(Long id);

    /***
     * @description: 修改标签信息
     * @param id
     */
    LabelInfoVO updataLabel(Long id, LabelParams labelParams);

    /***
     * @description: 添加歌曲到标签
     * @param musicLabelList
     */

    LabelInfoVO addMusicToLabel(List<LabelMusic> musicLabelList);

    /***
     * @description: 删除标签中的歌曲
     * @param musicLabelList
     */
    LabelInfoVO delMusicToLabel(List<LabelMusic> musicLabelList);
}
