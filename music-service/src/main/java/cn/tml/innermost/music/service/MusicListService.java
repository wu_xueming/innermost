package cn.tml.innermost.music.service;

import cn.tml.innermost.music.dos.MusicList;
import cn.tml.innermost.music.params.MusicSearchParams;
import cn.tml.innermost.music.vo.MusicListVO;
import cn.tml.innermost.music.vo.MusicListInfoVO;
import cn.tml.innermost.music.params.MusicListParams;
import cn.tml.innermost.music.vo.MusicListPageVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;


/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface MusicListService extends IService<MusicList> {

    /**
     * 获取歌单信息
     *
     * @param MusicListId
     * @return MusicListInfoVO
     */
    MusicListInfoVO getMusicListInfo(Long MusicListId);

    /***
     * @description: 分页获取所有歌单信息
     * @param current
     * @param size
     * @return: MusicListPageVO
     */
    MusicListPageVO getAllMusicListInfo(Long current, Long size);

    /**
     * 分页获取歌单所有歌曲
     * @param MusicListId
     * @return MusicListVO
     */
    MusicListVO getMusicListAllMusic(Long MusicListId);

    /**
     * 新建歌单
     *
     * @param MusicListParams
     * @return MusicListInfoVO
     */
    MusicListInfoVO addMusicList(MusicListParams MusicListParams);

    /**
     * 删除歌单
     *
     * @param MusicListId
     * @return MusicListInfoVO
     */
    void delMusicList(Long MusicListId);

    /***
     * 修改歌单信息
     * @param MusicListParams
     * @return: MusicListInfoVO
     */
    MusicListInfoVO updateMusicList(MusicListParams MusicListParams);

    /**
     * 搜索歌单
     * @param musicSearchParams
     * @return
     */
    List<MusicListInfoVO> selectMusicList(MusicSearchParams musicSearchParams);


    /**
     * 给歌单添加歌曲
     *
     * @param musicMusicLists
     * @return MusicListInfoVO
     */
    MusicListInfoVO addMusicToMusicList(Long musicId, Long musicListId);

    /**
     * 删除歌单中的歌曲
     *
     * @param musicMusicLists
     * @return MusicListInfoVO
     */
    MusicListInfoVO delMusicToMusicList(Long musicId, Long MusicListId);

    /**
     * 增加歌单播放量
     * @param MusicListId
     * @param cnt
     * @return url
     */

    Boolean addListenCount(Long MusicListId, int cnt);


    /**
     * 随机获取redis中播放量和收藏量前1000的歌单
     */
    List<MusicListInfoVO> getRandomMusicListFromTop1000(int count);

}
