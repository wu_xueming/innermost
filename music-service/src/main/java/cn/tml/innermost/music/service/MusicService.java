package cn.tml.innermost.music.service;

import cn.tml.innermost.music.dos.Music;
import cn.tml.innermost.music.params.HomaPageParams;
import cn.tml.innermost.music.params.MusicParams;
import cn.tml.innermost.music.params.MusicSearchParams;
import cn.tml.innermost.music.params.SelectMusicWithKeyParams;
import cn.tml.innermost.music.vo.HomePageVO;
import cn.tml.innermost.music.vo.MusicBaseInfoPageVO;
import cn.tml.innermost.music.vo.MusicInfoVO;
import cn.tml.innermost.music.vo.MusicPageVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface MusicService extends IService<Music> {

    /**
     * 根据id获取歌曲
     * @param musicId
     * @return: MusicInfoVO
     */
    MusicInfoVO getMusic(Long musicId);

    /**
     * 批量获取music
     */
    List<MusicInfoVO> selectMusicList(List<Long> musicIdList);

    /**
     * 指定字段，值与数量，批量获取
     */
    List<MusicInfoVO> selectMusicListWithColumn(SelectMusicWithKeyParams params);

    /***
     * @description: 分页获取所有歌曲
     * @param current
     * @param size
     * @return: MusicPageVO
     */
    MusicPageVO getAllMusic(Long current, Long size);

    /***
     * @description: 搜索歌曲
     * @param musicSearchParams
     * @return: MusicPageVO
     */
    List<MusicInfoVO> search(MusicSearchParams musicSearchParams);

    /***
     * 增加歌曲的播放量
     * @param musicId
     * @return: OK
     */
    void addMusicPlayNums(Long musicId);

    /**
     * 新建一首歌曲
     * @param musicParams
     * @return: MusicInfoVO
     */
    MusicInfoVO addMusic(MusicParams musicParams);

    /**
     * 删除一首歌曲
     * @param musicId
     * @return: MusicInfoVO
     */
    MusicInfoVO delMusic(Long musicId);

    /**
     * 修改歌曲信息
     * @param musicId
     * @param musicParams
     * @return: MusicInfoVO
     */
    MusicInfoVO updateMusic(Long musicId, MusicParams musicParams);

    /**
     * 根据id获取音乐链接
     * @param id
     * @return
     */
    MusicInfoVO getMusicUrl(Long id);

    /**
     * 主要推荐
     * @author 燧枫
     * @date 2023/5/5 13:28
    */
    HomePageVO getHomePage(HomaPageParams homaPageParams);

    /**
     * 增加歌曲播放量
     * @param musicId
     * @return
     */
    MusicInfoVO incrMusicListenCount(Long musicId,int count);

    String getMusicLyrics(Long musicId);
}
