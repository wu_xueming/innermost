package cn.tml.innermost.music.service;

import cn.tml.innermost.music.dos.SingerMusic;
import cn.tml.innermost.music.dos.Singer;
import cn.tml.innermost.music.dos.SingerAlbum;
import cn.tml.innermost.music.params.MusicSearchParams;
import cn.tml.innermost.music.params.SingerParams;
import cn.tml.innermost.music.params.SingerSearchParams;
import cn.tml.innermost.music.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 燧枫
 * @since 2022-10-18
 */
public interface SingerService extends IService<Singer> {

    List<SingerInfoVO> search(MusicSearchParams musicSearchParams);

    /**
     * 获取歌手信息
     *
     * @param SingerId
     * @return MusicListInfoVO
     */
    SingerInfoVO getSingerInfo(Long SingerId);

    /***
     * 分页获取所有歌手
     * @param 当前页码 current
     * @param 每页条数 size
     * @return: SingerPageVO
     */
    SingerPageVO getAllSingerInfo(Long current, Long size);

    // 快速建立歌手与歌曲的关系表
    void creatSingerAndMusic(Long current, Long size);

    /***
     * @description: 获取歌手所有歌曲
     * @param singerId
     * @return: SingerPageVO
     */
    SingerMusicVO getSingerAllMusic(Long singerId, Long current, Long size);

    /***
     *  获取歌手所有专辑
     *
     * @param singerId
     * @return SingerAlbumVO
     */
    SingerAlbumVO getSingerAllAlbum(Long singerId, Long current, Long size);

    /***
     *  新建一个歌手
     * @param singerParams
     * @return: SingerInfoVO
     */
    SingerInfoVO addSinger(SingerParams singerParams);

    /***
     *  删除一个歌手
     * @param singerId
     * @return: SingerInfoVO
     */
    SingerInfoVO delSinger(Long singerId);

    /***
     * 修改歌手信息
     * @param singerId
     * @param singerParams
     * @return: SingerInfoVO
     */
    SingerInfoVO updataSinger(Long singerId, SingerParams singerParams);

    /***
     *  添加歌曲到歌手
     * @param singerMusicList
     * @return: SingerInfoVO
     */
    SingerInfoVO addMusicToSinger(List<SingerMusic> singerMusicList);

    /***
     *  删除歌手的歌曲
     * @param singerMusicList
     * @return: SingerInfoVO
     */
    SingerInfoVO delMusicToSinger(List<SingerMusic> singerMusicList);

    /***
     *  添加专辑到歌手
     * @param singerAlbumList
     * @return:
     */
    SingerInfoVO addAlbumToSinger(List<SingerAlbum> singerAlbumList);

    /***
     *  删除歌手中的专辑
     * @param singerAlbumList
     * @return:
     */
    SingerInfoVO delAlbumToSinger(List<SingerAlbum> singerAlbumList);

    /**
     * 随机获取redis中的前100粉丝数的歌
     */
    List<SingerInfoVO> getRandomSingerFromTop100(int count);
}
