package cn.tml.innermost.music.service.impl;

import cn.tml.innermost.music.entity.pachong.JsonRootBean;
import cn.tml.innermost.music.entity.pachong.MusicURL;
import cn.tml.innermost.music.utils.QQMusicRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.downloader.HttpClientDownloader;

@Component
public class MusicUrlService {

    private static String cookie = "RK=HaF5mN38eF; ptcz=d3743e4be55b8f8513e6a5c1956f3a972f8f792b5a5cef1b527a077559101fe0; pgv_pvid=997503600; tvfe_boss_uuid=5a03ef83cef9a120; o_cookie=369202865; pac_uid=1_369202865; eas_sid=v1w6t880p3f0C8I9z7N1k2w475; fqm_pvqid=e8a434ee-0ec6-47d7-8252-4859ec5e3fdd; ts_uid=6143663293; tmeLoginType=2; ptui_loginuin=3250162035; euin=oi-koe6sowni7v**; fqm_sessionid=fd674599-bb7b-46ec-8e81-87989597b0b5; pgv_info=ssid=s3306782992; ts_refer=cn.bing.com/; _qpsvr_localtk=0.15721340019679664; login_type=1; psrf_musickey_createtime=1684151870; qqmusic_key=Q_H_L_5LUXw4-5vW4U0yoRp1pyE6ZZJeqXEX5XMdLV07IJKna1DPYRq9bgDzg; uin=3250162035; wxunionid=; psrf_qqaccess_token=0499E27626EA0CF3A836A670600FD793; qm_keyst=Q_H_L_5LUXw4-5vW4U0yoRp1pyE6ZZJeqXEX5XMdLV07IJKna1DPYRq9bgDzg; wxopenid=; wxrefresh_token=; psrf_access_token_expiresAt=1691927870; psrf_qqrefresh_token=E6BA4A4BD0A05ACE7A4A9084EC584692; qm_keyst=Q_H_L_5LUXw4-5vW4U0yoRp1pyE6ZZJeqXEX5XMdLV07IJKna1DPYRq9bgDzg; psrf_qqunionid=F2B1FB5346566131EA206508AD447E82; psrf_qqopenid=C3F6FC031F43D72371DD023865E9DFBA; ts_last=y.qq.com/n/ryqq/player";

    private final String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.48";

    private static String qqURLPrefix = "https://dl.stream.qqmusic.qq.com/";

    public String getMusicUrlBySongMid(String songMid) {
        JsonRootBean jsonRootBean = new JsonRootBean();
        jsonRootBean.setReq_1(new MusicURL(new String[]{songMid}));
        Request request = QQMusicRequest.getQQMusicRequest(jsonRootBean);
        request.addHeader("cookie", cookie);
        request.putExtra("requestType", "musicURL");

        // 使用WebMagic发送请求并获取响应
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        Page page = httpClientDownloader.download(request, new Task() {
            @Override
            public String getUUID() {
                return "your-uuid";
            }

            @Override
            public Site getSite() {
                return Site.me().setUserAgent(userAgent).setSleepTime(100)
                        .setRetrySleepTime(100).setRetryTimes(3);
            }
        });

        // 解析响应数据
        JSONObject data = JSON.parseObject(page.getRawText()).getJSONObject("req_1").getJSONObject("data");
        JSONArray midurlinfo = data.getJSONArray("midurlinfo");
        JSONObject temp = midurlinfo.getJSONObject(0);
        String purl = temp.getString("purl");
        if (purl.equals("")) {
            return null;
        }
        return qqURLPrefix + purl;
    }
}
