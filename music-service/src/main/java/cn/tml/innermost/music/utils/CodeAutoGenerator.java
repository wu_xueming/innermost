package cn.tml.innermost.music.utils;



public class CodeAutoGenerator {
    public static void main(String[] args) {

//        // 生产文件的项目相对位置
//        StringBuffer projectPath = new StringBuffer();
//        // 获取系统路径
//        String systemPath = System.getProperty("user.dir");
//        // 将反斜杠全部替换为双斜杠 并拼接项目路径
//        projectPath.append(systemPath.replaceAll("\\\\", "//"));
//        System.out.println(projectPath);
//
//        //entity表字段属性List，用于生成时间自动填充属性
//        List<IFill> iFills = new ArrayList<>();
//        iFills.add(new Column("create_time", FieldFill.INSERT));
//        iFills.add(new Column("update_time", FieldFill.INSERT_UPDATE));
//
//        FastAutoGenerator.create("jdbc:mysql://localhost:3306/innermost_music?verifyServerCertificate=false&useSSL=false", "root", "123456")
//                .globalConfig(builder -> {
//                    builder
//                            .author("燧枫") // 设置作者
////                            .enableSwagger() // 开启 swagger 模式
//                            .fileOverride() // 覆盖已生成文件
//                            .outputDir(projectPath + "//music-service//src//main//java")// 指定输出目录
//                            .disableOpenDir();// 生成代码后不打开输出目录
//                })
//                .packageConfig(builder -> {
//                    builder.parent("cn.tml.innermost") // 设置父包名
//                            .moduleName("music") // 设置父包模块名
//                            .entity("entity.dos")
//                            .controller("controller") // Controller 包名	默认值:controller
//                            .other("other") // 自定义文件包名	输出自定义文件时所用到的包名
//                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "//music-service//src//main//resources//mapper")); // 设置mapperXml生成路径
//                })
//                .strategyConfig(builder -> {
//                    builder.addInclude(getTables(scanner("请输入表名，多个英文逗号分隔？所有输入 all:"))) // 设置需要生成的表名
//                            .addTablePrefix("mt_")// 设置过滤表前缀
//                            .entityBuilder().addTableFills(iFills)//生成时间自动填充属性
//                            .controllerBuilder().enableRestStyle()//开启@RestController风格
//                            .serviceBuilder().formatServiceFileName("%sService"); //去掉默认的I前缀
//                })
//                //使用Freemarker引擎模板，默认的是Velocity引擎模板
//                .templateEngine(new FreemarkerTemplateEngine())
//                //设置自定义模板路径
//                .templateConfig(builder -> builder.controller("/templates/controller.java"))
//                .execute();
//
//    }
//
//    /**
//     * 读取控制台内容
//     */
//    public static String scanner(String tip) {
//        Scanner scanner = new Scanner(System.in);
//        StringBuilder help = new StringBuilder();
//        help.append(tip);
//        System.out.println(help.toString());
//        if (scanner.hasNext()) {
//            String ipt = scanner.next();
//            if (
//                    StringUtils.isNotBlank(ipt)) {
//                return ipt;
//            }
//        }
//        throw new MybatisPlusException("请输入正确的" + tip + "！");
//    }
//
//    /**
//     * 处理 all 情况
//     */
//
//    protected static List<String> getTables(String tables) {
//        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }

}
