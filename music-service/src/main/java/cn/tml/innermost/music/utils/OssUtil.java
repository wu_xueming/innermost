package cn.tml.innermost.music.utils;

import com.aliyun.oss.OSSClient;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;


public class OssUtil {

    static String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
    static String accessKeyId = "LTAI5tRc7Kj2NDShzofCT6A9";
    static String accessKeySecret = "SVPcqecSSsCmwEfDTyJrIQWufeHePe";
    static String bucketName = "timemachinelab";
    static String musicToOssPre = "InnerMost/Music/";

    // 上传文件
    public static String saveToOss(File file, String FileName) {
        String ObjectName = musicToOssPre + FileName;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, ObjectName, file);
        long expiration = new Date().getTime() + 3600l * 1000 * 24 * 365 * 10;
        Date expirationDate = new Date(expiration);
        URL url = ossClient.generatePresignedUrl(bucketName, ObjectName, expirationDate);
        ossClient.shutdown();
        delLinuxFile(new File(ThreadDownloader.downLoadAddress));
        return url.toString();
    }

    public static String SaveToOss(InputStream inputStream, String FileName, String prefix) {
        String ObjectName = prefix + ":" + FileName;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        ossClient.putObject(bucketName, ObjectName, inputStream);
        long expiration = new Date().getTime() + 3600L * 1000 * 24 * 365 * 10;
        Date expirationDate = new Date(expiration);
        URL url = ossClient.generatePresignedUrl(bucketName, ObjectName, expirationDate);
        ossClient.shutdown();
        return url.toString();
    }

    public static boolean delLinuxFile(File file) {
//        boolean flag = false;
//        if (file.exists() && file.isFile() && file.delete())
//            flag = true;
//        else
//            flag = false;
//        return flag;
        //判断文件不为null或文件目录存在
        if (file == null || !file.exists()){
            return false;
        }
        //取得这个目录下的所有子文件对象
        File[] files = file.listFiles();
        //遍历该目录下的文件对象
        for (File f: files){
            //打印文件名
            String name = file.getName();
            System.out.println(name);
            //判断子目录是否存在子目录,如果是文件则删除
            if (f.isDirectory()){
                delLinuxFile(f);
            }else {
                f.delete();
            }
        }
        //删除空文件夹  for循环已经把上一层节点的目录清空。
        file.delete();
        return true;
    }

    public static void delWindowsFile(File file) {
        //判断文件不为null或文件目录存在
        if (file == null || !file.exists()){
            return;
        }
        //取得这个目录下的所有子文件对象
        File[] files = file.listFiles();
        //遍历该目录下的文件对象
        for (File f: files){
            //打印文件名
            String name = file.getName();
            System.out.println(name);
            //判断子目录是否存在子目录,如果是文件则删除
            if (f.isDirectory()){
                delWindowsFile(f);
            }else {
                f.delete();
            }
        }
        //删除空文件夹  for循环已经把上一层节点的目录清空。
        file.delete();
    }
}

