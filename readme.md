# 开发约束
1. 项目采用uuid作为用户设备的唯一标识，客户端访问接口的时候必须在请求头里面携带一个uuid字段，前后端开发的人员都需要注意这个
2. 项目采用双token机制（登录之后两个token也放在请求头中），提升用户使用体验
# 框架使用说明
### 登录鉴权
1. 登录鉴权使用Login注解，如下图所示，表示开启登录鉴权，并且需要管理员身份：\
  ![img_1.png](img/img_1.png)
2. 退出登录接口使用Logout注解注释，即可完成退出登录操作，接口内部无需写逻辑：\
  ![img_6.png](img/img_6.png)\
  举例，下图是普通用户退出登录的接口：\
  ![img_9.png](img/img_9.png)
3. 获取当前登录用户：\
   ![img_4.png](img/img_4.png)\
   举例：\
   ![img_5.png](img/img_5.png)
### DO类公共字段
- DO实体类必须要有下面几个字段，并且要用如下注解注释（可以继承BaseEntity直接获取）：\
  ![img_2.png](img/img_2.png)
### 业务中的错误处理
- 业务中的错误通过抛出ServiceException异常返回给前端（下图是抛出音乐保存失败异常）：\
  ![img_3.png](img/img_3.png)
# 框架工具类
### 邮箱验证码工具类：
1. 对外接口：\
![img_8.png](img/img_8.png)
2. 配置（默认使用QQ邮箱，端口号587，可以在配置里面更改）:\
![img_7.png](img/img_7.png)
# 后端任务
## 接口完成标准
1. 必须是合理的，且能正常使用的
2. 接口文档必须实时更新接口进度，且文档是清晰明了的
## 音乐模块
### 2022年10月19日
* 完成音乐、标签、歌单的增删查改接口
## 用户模块
### 2022年10月19日
* 完成手机验证码、邮箱验证码、用户注册、用户登录、用户信息修改、歌曲收藏、歌单收藏、创建歌单等接口
## 管理员模块
### 2022年10月19日
* 完成管理员登录、角色的增删查改接口