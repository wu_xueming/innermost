package cn.tml.innermost.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.music.vo.MusicInfoVO;
import cn.tml.innermost.service.RecommendService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/listener/recommend")
public class RecommendController {
    @Resource
    RecommendService recommendService;
    @GetMapping("/songs/{count}")
    public ResultMessage getRecommendSongs(@PathVariable Integer count){
        List<MusicInfoVO> recommendSongs = recommendService.getRecommendSongs(count);
        return ResultUtil.data(recommendSongs);
    }
}
