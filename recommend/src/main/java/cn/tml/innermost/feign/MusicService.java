package cn.tml.innermost.feign;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.music.params.SelectMusicWithKeyParams;
import cn.tml.innermost.music.vo.MusicInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@FeignClient(value = "music-service",path = "listener/music")
@Component
public interface MusicService {

    @PostMapping("/list")
    public ResultMessage<List<MusicInfoVO>> selectMusicList(@RequestBody List<Long> musicIdList);

    @PostMapping("/listWithColumn")
    public ResultMessage<List<MusicInfoVO>> selectMusicListWithColumn(SelectMusicWithKeyParams params);
}
