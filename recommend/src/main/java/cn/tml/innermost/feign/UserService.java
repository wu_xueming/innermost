package cn.tml.innermost.feign;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.user.vos.params.PageParams;
import cn.tml.innermost.user.dos.MusicRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@FeignClient(value = "user-service",path = "listener/user")
@Component("userService")
public interface UserService {

    /**
     * 获取用户的歌曲播放历史
     * @param musicRecordParams
     * @return
     */
    @PostMapping("playingRecord")
    public ResultMessage<List<MusicRecord>> playingRecord(@Validated @RequestBody PageParams musicRecordParams);


}
