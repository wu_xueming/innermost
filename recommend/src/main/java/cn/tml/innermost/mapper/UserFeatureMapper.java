package cn.tml.innermost.mapper;

import cn.tml.innermost.recommend.dos.UserFeature;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
public interface UserFeatureMapper extends BaseMapper<UserFeature> {
}
