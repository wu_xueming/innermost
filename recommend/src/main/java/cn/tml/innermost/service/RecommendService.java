package cn.tml.innermost.service;

import cn.tml.innermost.music.vo.MusicInfoVO;
import cn.tml.innermost.music.vo.MusicListInfoVO;

import java.util.List;

public interface RecommendService {

    public List<MusicInfoVO> getRecommendSongs(Integer musicNum);

    public List<MusicListInfoVO> getRecommendMusicList(Long userId,Integer musicListNum);


}
