package cn.tml.innermost.user.config;

import cn.tml.innermost.framework.security.context.UserContext;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("accessToken", UserContext.getCurrentUserToken());
    }
}
