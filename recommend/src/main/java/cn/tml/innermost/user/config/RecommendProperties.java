package cn.tml.innermost.user.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "recommend")
public class RecommendProperties {
//    推荐数量
    public int  recommendSongCount;

//    相同风格推荐数量
    public int sameGenresCount = 10;

//    相同语言推荐数量
    public int sameLanguageCount = 10;

//    相同作者推荐数量
    public int sameSingerCount = 10;

}
