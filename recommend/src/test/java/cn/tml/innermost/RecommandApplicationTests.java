package cn.tml.innermost;

import cn.tml.innermost.user.vos.params.PageParams;
import cn.tml.innermost.feign.UserService;
import cn.tml.innermost.service.RecommendService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class RecommandApplicationTests {

    @Resource
    RecommendService recommendService;
    @Resource
    UserService userService;
    @Test
    void contextLoads() {
//        recommendService.getRecommendSongs(1592838663321153538l,30);
        PageParams pageParams = new PageParams(1,50);

        System.out.println(userService.playingRecord(pageParams));
    }

}
