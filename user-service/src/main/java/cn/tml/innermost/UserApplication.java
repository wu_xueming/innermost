package cn.tml.innermost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@EnableAsync
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication(scanBasePackages = "cn.tml.innermost")
@EnableTransactionManagement
@EnableScheduling
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(DomainApplication.class, args);
    }
}