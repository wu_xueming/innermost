package cn.tml.innermost.user.cache.analysis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author welsir
 * @date 2023/3/23 10:32
 * @description: 默认key生成策略:直接获取参数拼接
 */
@Service
@Slf4j
public class DefaultKeyGenerateService extends IkeyGenerateService {

    private final String defaultConnector = "-";

    @Override
    public String buildKey(Class<?>[] parameterTypes, Object[] argument,List<String> keys,String preKey) {

        StringBuffer sb = new StringBuffer();
        //先将用户自定义key追加在redis中分目录存储
        sb.append(preKey);
        for (String k : keys) {
            int index = Integer.parseInt(k);
            if(sb.toString().equals(preKey)){
                sb.append(":");
            }else {
                sb.append("-");
            }
            sb.append(argument[index-1]);
        }
        return sb.toString();
    }
}
