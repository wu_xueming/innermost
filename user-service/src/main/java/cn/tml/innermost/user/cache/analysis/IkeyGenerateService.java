package cn.tml.innermost.user.cache.analysis;

import java.util.List;

/**
 * @author welsir
 * @date 2023/3/24 8:40
 * @description: key生成抽象类 定义生成key逻辑
 */
public abstract class IkeyGenerateService {

    /** 
     * @description:  获取动态key
     * @param:  
     * @return:  
     * @author: WELSIR
     */
    public String getKey(Class<?>[] parameterTypes,
                         Object[] argument,
                         List<String> keys,
                         String preKey){
        StringBuffer sb = new StringBuffer("");

        String key = buildKey(parameterTypes,argument,keys,preKey);
        sb.append(key);
        return sb.toString();
    }

    public abstract String buildKey(Class<?>[] parameterTypes,
                                    Object[] argument,
                                    List<String> keys,
                                    String preKey);
}
