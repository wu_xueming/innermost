package cn.tml.innermost.user.cache.analysis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author welsir
 * @date 2023/3/24 8:47
 * @description: 生成key中心生成对应策略实例
 */
@Service
@Slf4j
public class KeyGenerator {

    private ConcurrentHashMap<String, IkeyGenerateService> keyGenerateMap = new ConcurrentHashMap<>();

    public <T extends Annotation> String getKey(T annotation, Class<?>[] parameterTypes, Object[] args) {

        if (annotation != null) {
            Class<?> clazz = annotation.getClass();
            try {
                Method method = clazz.getMethod("key");
                String key = (String) method.invoke(annotation);
                Method method1 = clazz.getMethod("preKey");
                String preKey = (String) method1.invoke(annotation);
                List<String> keys = extractValues(key);
                if(isContainNumber(keys.get(0))){
                    return preKey+":"+key;
                }else if(!isAllNumber(keys)){
                    return new PropertyKeyGenerateService().getKey(parameterTypes,args,keys,preKey);
                }else {
                    return new DefaultKeyGenerateService().getKey(parameterTypes,args,keys,preKey);
                }
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private List<String> extractValues(String value) {
        if (null == value || value.equals("")) {
            return null;
        }
        String regex = "(?:\\w+|\\{\\d+\\})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);

        List<String> results = new ArrayList<String>();
        while (matcher.find()) {
            String matchedValue = matcher.group();
            if (matchedValue.startsWith("{") && matchedValue.endsWith("}")) {
                matchedValue = matchedValue.substring(1, matchedValue.length() - 1);
            }
            results.add(matchedValue);
        }
        log.info("generateKeyList::" + results);
        return results;
    }

    private boolean isContainNumber(String value) {
        String regex = "^[^0-9]*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    private boolean isAllNumber(List<String> list) {
        String regex = "^\\d+$";
        Pattern pattern = Pattern.compile(regex);
        for (String s : list) {
            Matcher matcher = pattern.matcher(s);
            if (!matcher.find()) {
                return false;
            }
        }
        return true;
    }


}
