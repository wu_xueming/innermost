package cn.tml.innermost.user.cache.analysis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author welsir
 * @date 2023/3/28 20:35
 * @description: 属性key生成策略
 */
@Service
@Slf4j
public class PropertyKeyGenerateService extends IkeyGenerateService {

    @Override
    public String buildKey(Class<?>[] parameterTypes, Object[] argument, List<String> keys, String preKey) {
        StringBuffer sb = new StringBuffer("");
        sb.append(preKey);
        try {
            for (int i = 0; i < keys.size(); i++) {
                int index = Integer.parseInt(keys.get(i))-1;
                String flied = keys.get(++i);
                if (sb.toString().equals(preKey)) {
                    sb.append(":");
                } else {
                    sb.append("-");
                }
                sb.append(getPropertyKey(sb,argument[index],flied));
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    private Object getPropertyKey(StringBuffer tmpKey,Object obj,String parameterFlied){
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if(field.getName().equals(parameterFlied)){
                String getterMethodName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                try {
                    Method method = clazz.getMethod(getterMethodName);
                    return method.invoke(obj);
                } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
