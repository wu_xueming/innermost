package cn.tml.innermost.user.cache.annotation;

import cn.tml.innermost.user.cache.analysis.DefaultKeyGenerateService;
import cn.tml.innermost.user.cache.analysis.IkeyGenerateService;

import java.lang.annotation.*;

/**
 * @description: 缓存get
 * @author: welsir
 */
// 作用到类，方法，接口上等
@Target(ElementType.METHOD)
// 在运行时可以获取
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cache {

    /**
     * @description: 前缀key 默认为空 用于缓存分层
     */
    String preKey() default "";

    /**
     * @description: 主key 输入格式:"{1}{2}{3}...  (第一个参数 第二个参数 第三个参数 以此类推..)"
     */
    String key() default "";

    /**
     * @description: 过期时间(min)
     */
    int expire() default 720;

    /**
     * @description: 是否使用默认key生成策略 (支持自定义扩展 继承IKeyGenerateService)
     */
    Class<? extends IkeyGenerateService> generator() default DefaultKeyGenerateService.class;

    /**
     * @description: 注解类返回类型是否为默认基类Object
     */
    Class[] resultClass() default Object.class;
}
