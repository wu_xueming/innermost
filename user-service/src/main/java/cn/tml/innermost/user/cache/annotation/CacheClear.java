package cn.tml.innermost.user.cache.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
// 在运行时可以获取
@Retention(RetentionPolicy.RUNTIME)
@Documented
/*
 * @description: 缓存clear
 * @author: welsir
 */
public @interface CacheClear {

    /**
     * @description: 前缀key 默认为空 用于缓存分层
     */
    String preKey() default "";

    /**
     * @description: 主key 输入格式:"{1}{2}{3}...  (第一个参数 第二个参数 第三个参数 以此类推..)"
     */
    String key() default "";

}
