package cn.tml.innermost.user.cache.annotation;

import cn.tml.innermost.user.cache.analysis.DefaultKeyGenerateService;
import cn.tml.innermost.user.cache.analysis.IkeyGenerateService;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
// 在运行时可以获取
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * @description: 缓存update
 * @author: welsir
 */
public @interface CachePut {

    /**
     * @description: 前缀key 默认为空 用于缓存分层
     */
    String preKey() default "";

    /**
     * @description: 主key 输入格式:"{1.xx}{2.xx}{3.xx}...  (第一个参数 第二个参数 第三个参数 以此类推..)"
     */
    String key() default "";

    /**
     * @description: 过期时间(min)
     */
    int expire() default 720;

    /**
     * @description: 是否使用默认key生成策略 (支持自定义扩展 继承 IKeyGenerateService)
     */
    public Class<? extends IkeyGenerateService> generator() default DefaultKeyGenerateService.class;

    /**
     * @description: 注解类返回类型是否为默认基类Object
     */
    public Class[] resultClass() default Object.class;

}
