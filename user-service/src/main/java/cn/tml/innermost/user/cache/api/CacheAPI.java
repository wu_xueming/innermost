package cn.tml.innermost.user.cache.api;

/**
 * @author welsir
 * @date 2023/3/23 11:35
 */

import org.springframework.stereotype.Component;

/**
 * @description: 缓存对外暴露api
 * @author: welsir
 */
@Component
public interface CacheAPI {
    String get(String key);

    void set(String key, Object value, int exp);

    void set(String key, Object value);

    Long remove(String... keys);

    boolean isEnabled();

    String addSysKey(String key);

    String addUserKey(String key);
}
