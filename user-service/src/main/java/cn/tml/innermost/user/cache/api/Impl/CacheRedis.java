package cn.tml.innermost.user.cache.api.Impl;

import cn.tml.innermost.user.cache.api.CacheAPI;
import cn.tml.innermost.user.cache.config.RedisConfig;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
@Slf4j
/**
 * @description: 缓存操作具体实现
 * @author: welsir
 */
public class CacheRedis implements CacheAPI {

    @Resource
    private RedisConfig redisConfig;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public String get(String key) {
        key = addSysKey(key);
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void set(String key, Object value, int exp) {
        if (StringUtils.isBlank(key) || value == null || !isEnabled()) {
            return;
        }
        String reaValue = "";
        // Object value-> String value
        if (value instanceof String) {
            reaValue = value.toString();
        } else {
            reaValue = JSON.toJSONString(value, false);
        }
        // 添加模块key 即配置文件中key
        key = addSysKey(key);
        redisTemplate.opsForValue().set(key, reaValue, exp, TimeUnit.MINUTES);
    }

    @Override
    public void set(String key, Object value) {
    }

    @Override
    public Long remove(String... keys) {
        if (keys.length == 0 || !isEnabled())
            return 0L;
        List<String> keyList = new ArrayList<>();
        for (String key : keys) {
            keyList.add(addSysKey(key));
        }
        return redisTemplate.delete(keyList);
    }

    @Override
    public boolean isEnabled() {
        return redisConfig.getEnable();
    }

    @Override
    public String addSysKey(String key) {
        String systemKey = redisConfig.getSysName();
        if (key.startsWith(systemKey)) {
            return key;
        } else {
            return systemKey + ":" + key;
        }
    }

    @Override
    public String addUserKey(String key) {
        String userKey = redisConfig.getUserKey();
        if (key.startsWith(userKey)) {
            return key;
        } else {
            return userKey + ":" + key;
        }
    }
}
