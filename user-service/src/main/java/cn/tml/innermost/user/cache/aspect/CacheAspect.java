package cn.tml.innermost.user.cache.aspect;

import cn.tml.innermost.user.cache.analysis.KeyGenerator;
import cn.tml.innermost.user.cache.annotation.Cache;
import cn.tml.innermost.user.cache.api.CacheAPI;
import cn.tml.innermost.user.cache.paraser.DefaultResultParser;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.lang.reflect.Type;


@Service
@Aspect
@Slf4j
/**
 * @description: AOP切面
 * @author: welsir
 */
public class CacheAspect {

    @Resource
    private CacheAPI cacheAPI;
    @Resource
    private DefaultResultParser defaultResultParser;
    @Resource
    private KeyGenerator keyGenerator;

    @Pointcut("@annotation(cn.tml.innermost.user.cache.annotation.Cache)")
    public void aspect() {
    }

    @Around("aspect()&&@annotation(cache)")
    public Object interceptor(ProceedingJoinPoint joinPoint, Cache cache) throws Throwable {
        //环绕切面 拿到对应注解上方法的返回值 返回类型 方法名等
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Class<?>[] parameterTypes = signature.getParameterTypes();
        Method method = signature.getMethod();
        Type returnType = method.getGenericReturnType();
        Object[] args = joinPoint.getArgs();
        Object result = "";
        String key = "";
        try {
            key = keyGenerator.getKey(cache, parameterTypes, args);
            String value = cacheAPI.get(key);
            log.info("cacheTime"+cache.expire());
            if (value == null || value.equals("")) {
                result = joinPoint.proceed();
                cacheAPI.set(key, result, cache.expire());
            } else {
                cacheAPI.set(key,value,cache.expire());
                result = defaultResultParser.getResult(value, returnType, cache);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        log.info("cache result key-->value :: {"+ key+"}-->"+result);
        return result;
    }
}