package cn.tml.innermost.user.cache.aspect;

import cn.tml.innermost.user.cache.analysis.KeyGenerator;
import cn.tml.innermost.user.cache.annotation.CacheClear;
import cn.tml.innermost.user.cache.api.CacheAPI;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description: AOP切面
 * @author: welsir
 */
@Service
@Aspect
@Slf4j
public class CacheClearAspect {
    @Resource
    private CacheAPI cacheAPI;
    @Resource
    private KeyGenerator keyGenerator;

    @Pointcut("@annotation(cn.tml.innermost.user.cache.annotation.CacheClear)")
    public void aspect() {
    }

    @AfterReturning(value = "aspect()&&@annotation(cacheClear)",
            returning = "res")
    public Object afterRunningInterceptor(JoinPoint joinPoint, CacheClear cacheClear, Object res) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Class<?>[] parameterTypes = signature.getParameterTypes();
        Object[] args = joinPoint.getArgs();
        try {
            String key = keyGenerator.getKey(cacheClear, parameterTypes, args);
            Long count = cacheAPI.remove(key);
            log.info("cache remove count :: "+count);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        return res;
    }
}
