package cn.tml.innermost.user.cache.aspect;

import cn.tml.innermost.user.cache.analysis.KeyGenerator;
import cn.tml.innermost.user.cache.annotation.CachePut;
import cn.tml.innermost.user.cache.api.CacheAPI;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description: AOP切面
 * @author: welsir
 */
@Service
@Aspect
@Slf4j
public class CachePutAspect {

    @Resource
    private CacheAPI cacheAPI;
    @Resource
    private KeyGenerator keyGenerator;

    @Pointcut("@annotation(cn.tml.innermost.user.cache.annotation.CachePut)")
    public void aspect(){
    }

    @AfterReturning(value = "aspect()&&@annotation(cachePut)",
            returning = "res")
    public Object afterRunningInterceptor(JoinPoint joinPoint,Object res, CachePut cachePut){
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Class<?>[] parameterTypes = signature.getParameterTypes();
        Object[] args = joinPoint.getArgs();
        try {
            String key = keyGenerator.getKey(cachePut, parameterTypes, args);
            log.info("generateKey::"+key);
            String value = cacheAPI.get(key);
            //如果数据存在set的时候会不会覆盖
            if(value!=null){
                log.info("cache remove key-->value :: {"+key+"}-->"+value);
                cacheAPI.remove(key);
            }
            log.info("cache set msg::{"+key+"}"+"{"+value+"}"+"expTime::"+cachePut.expire());
            cacheAPI.set(key,res,cachePut.expire());
        }catch (RuntimeException e){
            throw new RuntimeException(e);
        }
        return res;
    }
}
