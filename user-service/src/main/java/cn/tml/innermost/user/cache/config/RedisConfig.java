package cn.tml.innermost.user.cache.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 实体类 存放配置文件信息
 * @author: welsir
 */
@Data
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfig {

    private RedisPoolConfig pool = new RedisPoolConfig();

    private String host;

    private String password;

    private int timeout;

    private int database;

    private int port;

    private Boolean enable;

    private String sysName;

    private String userKey;

    private Long refreshTimeout;


}
