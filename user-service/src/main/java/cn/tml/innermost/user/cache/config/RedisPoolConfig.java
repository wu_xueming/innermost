package cn.tml.innermost.user.cache.config;

import lombok.Data;

/**
 * @description: 连接池
 * @author: welsir
 */
@Data
public class RedisPoolConfig {
    private String maxActive;
    private String maxIdle;
    private String maxWait;
}
