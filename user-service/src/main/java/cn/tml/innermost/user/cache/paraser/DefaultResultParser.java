package cn.tml.innermost.user.cache.paraser;

import cn.tml.innermost.user.cache.annotation.Cache;
import com.alibaba.fastjson.JSON;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author welsir
 * @date 2023/3/23 20:11
 * @description: 将key转换成注解类的返回类型
 */
@Component
@Slf4j
public class DefaultResultParser {

    public Object getResult(String value, Type type, Cache cache){

        Class[] classes = cache.resultClass();

        Object result = null;
        try {
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type rawType = parameterizedType.getRawType();
                if (((Class) rawType).isAssignableFrom(List.class)) {
                    //fix 多层泛型引起的bug
                    if (parameterizedType.getActualTypeArguments()[0] instanceof ParameterizedType) {
                        result = JSON.parseArray(value, parameterizedType.getActualTypeArguments());
                    }
                    else {
                        result = JSON.parseArray(value, (Class) parameterizedType.getActualTypeArguments()[0]);
                    }
                }
            } else if (classes[0].equals(Object.class)) {
                result = JSON.parseObject(value).toJavaObject((Class) type);
            } else {
                result = JSON.parseObject(value).toJavaObject(classes[0]);
            }
        }catch (ClassCastException e){
            throw new ClassCastException(e.toString());
        }
        return result;
    }
}
