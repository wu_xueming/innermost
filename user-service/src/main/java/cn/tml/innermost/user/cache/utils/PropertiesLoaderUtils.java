package cn.tml.innermost.user.cache.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author welsir
 * @date 2023/3/23 8:56
 */
@Slf4j
public class PropertiesLoaderUtils {

    Properties properties;
    public PropertiesLoaderUtils(String resourcesPath) {
        String[] split = resourcesPath.split("\\.");
        if(split[split.length-1].equals("yml")){
            YamlPropertiesFactoryBean  yaml = new YamlPropertiesFactoryBean();
            ClassPathResource resource = new ClassPathResource(resourcesPath);
            yaml.setResources(resource);
            properties = yaml.getObject();
        }else {
            properties = new Properties();
            try(InputStream inputStream = new FileInputStream(resourcesPath)){
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getProperty(String s) {
        return properties.getProperty(s);
    }

}
