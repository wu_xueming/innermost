package cn.tml.innermost.user.controller;


import cn.tml.innermost.framework.entity.enums.ResultCode;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.security.annotations.Login;
import cn.tml.innermost.framework.security.annotations.Logout;
import cn.tml.innermost.framework.security.token.Token;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.user.dos.MusicRecord;
import cn.tml.innermost.user.dos.UserInfo;
import cn.tml.innermost.user.vos.params.*;
import cn.tml.innermost.user.vos.vo.*;
import cn.tml.innermost.user.service.UserService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/listener/user")
@Validated
public class UserController {

    @Resource
    private UserService userService;

    //用户注册
    @PostMapping("register")
    public ResultMessage<Token> register(@Validated
                                         @RequestBody RegisterParams registerParams) {
        Token token = userService.Register(registerParams);
        return ResultUtil.data(token);
    }

    //新增用户
    @PostMapping("addUser")
    public ResultMessage<Token> addUser(String userName, String password, String emailAddress) {
        userService.addUser(userName, password, emailAddress);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //用户通过账号登录
    @PostMapping("loginByUserName")
    public ResultMessage<Token> loginByUserName(@Validated
                                                @RequestBody LoginByUserNameVO loginByUserNameVO) {
        Token token = userService.Login(loginByUserNameVO);
        return ResultUtil.data(token);
    }

    //用户通过邮箱登录
    @PostMapping("loginByEmail")
    public ResultMessage<Token> loginByEmail(@Validated
                                             @RequestBody LoginByEmailVo loginByEmailVo) {
        Token token = userService.loginByEmail(loginByEmailVo);
        return ResultUtil.data(token);
    }


    //用户退出登录
    @Logout
    @DeleteMapping("logout")
    public ResultMessage loginOut() {
        return ResultUtil.success();
    }

    //用户修改信息
    @PostMapping("updateUserinfo")
    @Login(value = true)
    public ResultMessage<UpdateParams> update(@Validated
                                              @RequestBody UpdateParams updateParams) {
        UpdateParams user = userService.update(updateParams);
        return ResultUtil.data(user);
    }

    //获取用户信息
    @GetMapping("getUserInfo")
//    @Cache(key = "userInfo::")
    @Login(value = true)
    public ResultMessage<UserInfoVO> getUserInfo() {
        UserInfoVO user = userService.getUserInfo();
        return ResultUtil.data(user);
    }

    @GetMapping("getUserId")
    @Login(value = true)
    public ResultMessage getUserId(){
        return ResultUtil.data(userService.getUserId());

    }

    //用户对歌曲进行评论（暂未实现）
    @PostMapping("song")
//    @Login(value = true)
    public ResultMessage<UserInfo> comments(@Validated
                                                    CommentsParams commentsParams) {
        CommentsVO commentsVO = userService.comments(commentsParams);
        return ResultUtil.success(ResultCode.API_NOT_USE);
    }

    //用户选择主题
    @PostMapping("edit/theme")
    @Login(value = true)
    public ResultMessage<UserInfo> editTheme(@RequestBody ThemeVO themeVo) {
        userService.editTheme(themeVo);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //发送验证码到用户邮箱
    @PostMapping("sendEmailCode")
    public ResultMessage<UserInfo> sendEmailCode(@Validated
                                                 @RequestBody EmailParams emailParams) {
        userService.sendEmailCode(emailParams);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //用户找回密码
    @PostMapping("changePassword")
    public ResultMessage changePassword(@Validated
                                        @RequestBody ChangePasswordParams changePasswordParams) {
        userService.changePassword(changePasswordParams);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //删除用户
    @DeleteMapping("deleteUser")
    public ResultMessage deleteUser(@NotNull String userName) {
        userService.deleteUser(userName);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //查询用户分页
    @GetMapping("selectAllUser")
    public ResultMessage<Page<UserInfoVO>> selectAllUser(@NotNull Integer Page) {
        return ResultUtil.data(userService.selectUserPageVO(Page));
    }

    //查询单个用户
    @GetMapping("selectUser")
    public ResultMessage<UserInfoVO> selectUser(@NotNull @Length(min = 1, max = 15, message = "账号长度为1~15位") String userName) {
        return ResultUtil.data(userService.selectUser(userName));
    }

    //查询单个用户
    @GetMapping("selectUserById")
    public ResultMessage<UserInfoVO> selectUser(@JsonSerialize(using = ToStringSerializer.class) Long id) {
        return ResultUtil.data(userService.selectUser(id));
    }


    //修改单个用户信息
    @PostMapping("changeUserInfo")
    public ResultMessage changeUserInfo(@Validated
                                        @RequestBody ChangeUserInfoParams changeUserInfoParams) {
        userService.changeUserInfo(changeUserInfoParams);
        return ResultUtil.success(ResultCode.SUCCESS);
    }

    //用户上传头像
    @PostMapping("/profiles")
    @Login(value = true)
    public ResultMessage setUserProfile(@RequestParam("avatar") MultipartFile profile)  {
        System.out.println("用户上传头像中,文件名为+"+profile.getSize());
        String url = userService.updUserProfile(profile);
        return ResultUtil.data(url);
    }

    @PostMapping("playingRecord")
    public ResultMessage<List<MusicRecord>> playingRecord(@Validated  @RequestBody PageParams musicRecordParams) {
        return ResultUtil.data(userService.getPlayingRecord(musicRecordParams));
    }

    @DeleteMapping("deletePlayingRecord")
    @Login(value = true)
    public ResultMessage deletePlayingRecord(@RequestParam("musicName")
                                                 @JsonSerialize(using = ToStringSerializer.class) String musicName){
        userService.deletePlayingRecord(musicName);
        return ResultUtil.success();
    }

    @PostMapping("addPlayingRecord")
    @Login(value = true)
    public ResultMessage addPlayingRecord(@Validated
                                          @RequestBody MusicVO music) {
        userService.addMusicRecord(music);
        return ResultUtil.success();
    }

    @PostMapping("commentMusic")
    @Login(value = true)
    public ResultMessage commentMusic(@RequestBody CommentMusicParams commentMusicParams){
        userService.commentMusic(commentMusicParams);
        return ResultUtil.success();
    }

    @PostMapping("commentMusicList")
    @Login(value = true)
    public ResultMessage commentMusicList(@RequestBody CommentMusicListParams commentMusicListParams){
        userService.commentMusicList(commentMusicListParams);
        return ResultUtil.success();
    }

    @GetMapping("getMusicComment")
    public ResultMessage<Page<MusicCommentVO>> getMusicComment(@JsonSerialize(using = ToStringSerializer.class)Long musicId, int page, int size){
        return ResultUtil.data(userService.getMusicComment(musicId, page, size));
    }

    @GetMapping("getMusicListComment")
    public ResultMessage<Page<MusicListCommentVO>> getMusicListComment(@JsonSerialize(using = ToStringSerializer.class)Long MusicListId, int page, int size){
        return ResultUtil.data(userService.getMusicListComment(MusicListId, page, size));
    }

    @GetMapping("getMusicSubComment")
    public ResultMessage<Page<MusicCommentVO>> getMusicSubComment(@JsonSerialize(using = ToStringSerializer.class)Long rootId, int page, int size){
        return ResultUtil.data(userService.getMusicSubComment(rootId, page, size));
    }

    @GetMapping("getMusicListSubComment")
    public ResultMessage<Page<MusicListCommentVO>> getMusicListSubComment(@JsonSerialize(using = ToStringSerializer.class)Long rootId, int page, int size){
        return ResultUtil.data(userService.getMusicListSubComment(rootId, page, size));
    }

    @DeleteMapping("deleteComment")
    public ResultMessage deleteComment(@JsonSerialize(using = ToStringSerializer.class) Long id){
        userService.deleteComment(id);
        return ResultUtil.success();
    }

    @PostMapping("test")
    public ResultMessage redisTest(){
        userService.redisTest();
        return ResultUtil.success();
    }
}
