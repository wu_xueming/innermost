package cn.tml.innermost.user.controller;

import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.framework.security.annotations.Login;
import cn.tml.innermost.framework.security.context.UserContext;
import cn.tml.innermost.framework.utils.ResultUtil;
import cn.tml.innermost.user.dos.MusicListRecord;
import cn.tml.innermost.user.dos.UserMusicList;
import cn.tml.innermost.user.service.UserMusicListService;
import cn.tml.innermost.user.utils.UserInfoUtil;
import cn.tml.innermost.user.vos.params.PageParams;
import cn.tml.innermost.user.vos.vo.MusicListVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/16 21:21
 */
@RestController
@Validated
@Slf4j
@RequestMapping("/listener/user/musicList")
public class UserMusicListController {

    @Resource
    private UserMusicListService userMusicListService;

    @Resource
    private UserInfoUtil userInfoUtil;


    /**
     * @description: 用户新建歌单     测试成功
     * @param: [MusicListName]       歌单名称
     * @return: [MusicListVO]        歌单VO实体类
     * @author: WELSIR
     * @date: 2022/10/20 22:58
     */
    @Login(value = true)
    @PutMapping("")
    public ResultMessage insertMusicList(@NotBlank(message = "歌单名称不能为空!")
                                        @RequestParam("musicListName") String MusicListName) {
        return ResultUtil.data(userMusicListService.insertMusicList(MusicListName));
    }

    /**
     * @description: 添加收藏歌单     测试成功
     * @param: [MusicListId]     歌单Id
     * @return: [MusicListId]    歌单Id
     * @author: WELSIR
     * @date: 2022/10/25 21:24
     */
    @Login(value = true)
    @PutMapping("collect/{musicListId}")
    public ResultMessage insertCollectionMusicList(@NotNull(message = "歌单id不能为空!")
                                                  @PathVariable Long musicListId) {
        Boolean aBoolean = userMusicListService.insertCollectionMusicList(musicListId);
        if(aBoolean){
            return ResultUtil.data(true);
        }else {
            return ResultUtil.error(400,"该歌单已收藏");
        }
    }

    /**
     * @description: 新增歌曲到歌单(只准自建)     多首歌曲Id和一个歌单Id，封装成JSON对象
     * @param: [musicMusicListParams]        歌单列表对象，包括歌单Id和歌曲Id等字段
     * @return: [musicMusicListParams]       歌单列表对象，包括歌单Id和歌曲Id等字段
     * @author: WELSIR
     * @date: 2022/10/25 21:24
     */
    @Login(value = true)
    @PutMapping("songToMusicList/{musicId}/{musicListId}")
    public ResultMessage insertSongToMusicList(@Validated @PathVariable Long musicId,
                                              @Validated @PathVariable Long musicListId) {
        return ResultUtil.data(userMusicListService.insertSongToMusicList(musicId,musicListId));
    }

    /**
     * @description: 删除歌单   (是否需要伪删除)   测试成功
     * @param: [MusicListId]     歌单Id
     * @return: [MusicListId]    歌单Id
     * @author: WELSIR
     * @date: 2022/10/25 21:24
     */
    @Login(value = true)
    @DeleteMapping("{musicListId}")
    public ResultMessage deleteCollectionMusicList(@NotNull(message = "歌单id不能为空")
                                                  @PathVariable Long musicListId) {
        return ResultUtil.data(userMusicListService.deleteMusicList(musicListId));
    }

    /**
     * @description: 获取自建歌单列表   测试成功
     * @param:
     * @return: MusicListVOList  歌单VOList集合
     * @author: WELSIR
     * @date: 2022/10/22 17:20
     */
    @Login(value = true)
    @GetMapping("mine")
    public ResultMessage getUserMusicList() {
        Long userId = UserContext.getCurrentUser().getId();
        List<UserMusicList> userMusicList = userMusicListService.getUserMusicList(userId);
        return ResultUtil.data(userMusicList);
    }

    /**
     * @description: 获取收藏歌单列表
     * @param: []
     * @return:
     * @author: WELSIR
     * @date: 2022/10/23 18:55
     */
    @Login(value = true)
    @GetMapping("collect")
    public ResultMessage getCollectionMusicList() {
        Long userId = UserContext.getCurrentUser().getId();
        return ResultUtil.data(userMusicListService.getUserCollectionMusicList(userId));
    }

    /**
     * @description: 获取具体某个歌单
     * @param: []
     * @return: []
     * @author: WELSIR
     * @date: 2022/10/23 18:55
     */
    @Login()
    @GetMapping("oneMusicList/{MusicListId}")
    public ResultMessage getOneMusicList(@NotNull(message = "歌单id不能为空")
                                        @PathVariable Long MusicListId) {
        return ResultUtil.data(userMusicListService.getOneMusicList(MusicListId));
    }

    /***
     * @description: 修改歌单封面
     * @param id
     * @return: url
     */
    @PostMapping("updateCover/{id}")
    public ResultMessage updateMusicListCover(@PathVariable Long id,@RequestParam("avatar") MultipartFile file){
        MusicListVO vo = userMusicListService.updateMusicListCover(id,file);
        return ResultUtil.data(vo);
    }

    /**
     * 添加歌单浏览历史
     * @param musicListRecord
     * @return
     */
    @PostMapping("insertMusicListRecord")
    public ResultMessage insertMusicListRecord(@RequestBody MusicListRecord musicListRecord){
        if(userMusicListService.insertMusicListRecord(musicListRecord)){
            return ResultUtil.success();
        }else {
            return ResultUtil.error(500,"添加歌单历史失败");
        }
    }

    /**
     * 查询歌单浏览历史
     * @param pageParams
     * @return
     */
    @PostMapping("selectMusicListRecord")
    public ResultMessage selectMusicListRecord(@RequestBody PageParams pageParams){
        return ResultUtil.data(userMusicListService.selectMusicListRecord(pageParams));

    }

}