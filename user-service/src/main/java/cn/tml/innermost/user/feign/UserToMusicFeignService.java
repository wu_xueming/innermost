package cn.tml.innermost.user.feign;
import cn.tml.innermost.framework.entity.vo.ResultMessage;
import cn.tml.innermost.music.params.MusicListParams;
import cn.tml.innermost.music.vo.MusicInfoVO;
import cn.tml.innermost.music.vo.MusicListInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "music-service",path = "listener/music/musicList")
@Component
public interface UserToMusicFeignService {
//    用户添加歌单
    @PutMapping(value = "")
     ResultMessage<MusicListInfoVO> addMusicList(@RequestBody @Validated MusicListParams musicListParams);

//    用户删除歌单
    @DeleteMapping(value = "{id}")
     ResultMessage<MusicListInfoVO> delMusicList(@PathVariable Long id);

//    更新歌单信息
    @PostMapping(value = "")
     ResultMessage<MusicListInfoVO> updateMusicList(@RequestBody @Validated MusicListParams musicListParams);

//    给歌单添加歌曲
    @PutMapping(value = "songToMusicList/{musicId}/{musicListId}")
    ResultMessage<MusicListInfoVO> addMusicToMusicList(@Validated @PathVariable Long musicId,
                                                       @Validated @PathVariable Long musicListId);

//    删除歌单中的歌曲
    @DeleteMapping(value = "delMusicFromList/{musicId}/{MusicListId}")
    ResultMessage<MusicListInfoVO> delMusicInMusicList(@Validated @PathVariable Long musicId, @PathVariable Long MusicListId);

//    获取歌单信息
    @GetMapping(value = "MusicListInfo/{id}")
    ResultMessage<MusicListInfoVO> getMusicListInfo(@PathVariable Long id);

//    增加歌曲播放量
    @PostMapping("incrListenCount/{musicId}")
    MusicInfoVO incrMusicListenCount(@PathVariable Long musicId, int count);


}
