package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2023/4/9
 */
@Mapper
public interface CommentServerMapper extends BaseMapper<Comment> {
}
