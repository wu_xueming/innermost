package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.MusicListRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
public interface MusicListRecordMapper extends BaseMapper<MusicListRecord> {
}
