package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.CollectionMusicList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author welsir
 */
@Component
public interface UserCollectionMusicListMapper extends BaseMapper<CollectionMusicList> {
}
