package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.UserMusicList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
public interface UserMusicListMapper extends BaseMapper<UserMusicList> {

}
