package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.MusicRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserPlayRecordMapper extends BaseMapper<MusicRecord> {
}
