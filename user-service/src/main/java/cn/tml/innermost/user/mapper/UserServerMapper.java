package cn.tml.innermost.user.mapper;

import cn.tml.innermost.user.dos.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserServerMapper extends BaseMapper<UserInfo> {
}
