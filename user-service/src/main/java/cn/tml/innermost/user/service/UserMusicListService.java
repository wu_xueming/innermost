package cn.tml.innermost.user.service;

import cn.tml.innermost.music.params.MusicListParams;
import cn.tml.innermost.user.dos.CollectionMusicList;
import cn.tml.innermost.user.dos.MusicListRecord;
import cn.tml.innermost.user.dos.UserMusicList;
import cn.tml.innermost.user.vos.params.PageParams;
import cn.tml.innermost.user.vos.vo.MusicListRecordVO;
import cn.tml.innermost.user.vos.vo.MusicListVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author welsir
 */

public interface UserMusicListService {

    /**
     * @description:  用户注册时生成的默认歌单
     * @param: [MusicListName, userName, userId]
     * @return: [MusicListName, userName, userId]
     * @author: WELSIR
     * @date: 2022/11/9 17:00
     */
    MusicListVO getRegisterMusicList(String MusicListName,String userName,Long userId);

    /**
     * @description: 用户新建歌单
     * @param: []
     * @return: []
     * @author: WELSIR
     * @date: 2022/10/20 23:19
     */
    MusicListVO insertMusicList(String MusicListName);

    /**
     * @description: 用户新增收藏歌单
     * @param:
     * @return:
     * @author: WELSIR
     * @date: 2022/10/20 23:57
     */
    Boolean insertCollectionMusicList(Long MusicListId);

    /**
     * @description: 新增歌曲到歌单
     * @param: [id]
     * @return: [id]
     * @author: WELSIR
     * @date: 2022/10/21 0:35
     */
    Boolean insertSongToMusicList(Long musicId, Long musicListId);


    /**
     * @description: 删除歌单
     * @param: [MusicListId, loginName]
     * @return: [MusicListId, loginName]
     * @author: WELSIR
     * @date: 2022/10/22 14:45
     */
    Boolean deleteMusicList(Long musicListId);



    /**
     * @description:  获取用户自建歌单信息
     * @param: [MusicListId]
     * @return: [MusicListId]
     * @author: WELSIR
     * @date: 2022/10/22 17:21
     */
    List<UserMusicList> getUserMusicList(Long userId);

    /**
     * @description:  获取用户收藏歌单
     * @param: []
     * @return: []
     * @author: WELSIR
     * @date: 2022/10/23 20:35
     */
    List<CollectionMusicList> getUserCollectionMusicList(Long userId);

    /**
     * @description: 获取具体一个歌单
     * @author: welsir
     * @Date:
     */
    MusicListVO getOneMusicList(Long MusicListId);

    /**
     * @description: 修改歌单信息
     * @author: welsir
     * @Date:
     */
    Boolean updateMusicList(MusicListParams MusicListParams);

    /**
     * 更新歌单封面
     *
     * @param file
     * @return url
     */
    MusicListVO updateMusicListCover(Long id, MultipartFile file);


    /**
     * 添加歌单历史
     * @param musicListRecord
     * @return
     */
    boolean insertMusicListRecord(MusicListRecord musicListRecord);

    /**
     * 查询歌单历史
     * @param pageParams
     * @return
     */
    List<MusicListRecordVO> selectMusicListRecord(PageParams pageParams);
}