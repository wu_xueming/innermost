package cn.tml.innermost.user.service;

import cn.tml.innermost.framework.security.token.Token;
import cn.tml.innermost.user.dos.MusicRecord;
import cn.tml.innermost.user.vos.params.*;
import cn.tml.innermost.user.vos.vo.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface UserService {

    Token Register(RegisterParams registerParams);

    UserInfoVO getUserInfo();

    IdVO getUserId();

    UpdateParams update(UpdateParams updateParams);

    CommentsVO comments(CommentsParams commentsParams);

    void editTheme(ThemeVO themeVo);

    Token Login(LoginByUserNameVO loginVO);

    void sendEmailCode(EmailParams emailParams);

    void changePassword(ChangePasswordParams changePasswordParams);

    Token loginByEmail(LoginByEmailVo loginByEmailVo);

    void deleteUser(String UserName);

    Page<UserInfoVO> selectUserPageVO(Integer Page);

    void addUser(String userName, String password, String emailAddress);

    UserInfoVO selectUser(String userName);

    UserInfoVO selectUser(Long id);

    void changeUserInfo(ChangeUserInfoParams changeUserInfoParams);

    String updUserProfile(MultipartFile profile);

    List<MusicRecord> getPlayingRecord(PageParams musicRecordParams);

    void deletePlayingRecord(String musicName);

    void addMusicRecord(MusicVO music);

    void commentMusic(CommentMusicParams commentMusicParams);

    void commentMusicList(CommentMusicListParams commentMusicListParams);

    Page<MusicCommentVO> getMusicComment(Long musicId, int page, int size);

    Page<MusicListCommentVO> getMusicListComment(Long MusicListId, int page, int size);

    Page<MusicCommentVO> getMusicSubComment(Long rootId, int page, int size);

    Page<MusicListCommentVO> getMusicListSubComment(Long rootId, int page, int size);

    void deleteComment(Long id);

    boolean redisTest();
}
