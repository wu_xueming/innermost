package cn.tml.innermost.user.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateHelper {
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    // 将日期对象转换为字符串
    public static String toY_M_D_H_M_S(Date date) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return dtf.format(localDateTime);
    }

    // 将字符串日期转换为时间毫秒数
    public static Long toTs(String dateStr) {
        // Date === LocalDateTime  Calendar === Instant
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, dtf);
        return localDateTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }


}
