package cn.tml.innermost.user.utils;

import cn.tml.innermost.framework.properties.RestProperties;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author welsir
 * @version 1.0
 * @description: TODO
 * @date 2022/10/21 10:37
 */
@Component
public class RestPropertiesPath {
    @Resource
    private RestProperties restProperties;

    /**
     * @description:  用户新建歌单
     * @param: [obj]
     * @return: [obj]
     * @author: WELSIR
     * @date: 2022/10/21 16:55
     */
    public String addMusicListUrl(){
        return restProperties.getMusicServiceUrl()+"MusicList/MusicList";
    }
    /** 
     * @description:   用户添加歌曲到歌单
     * @param: [musicId] 
     * @return: [musicId] 
     * @author: WELSIR
     * @date: 2022/10/21 21:43
     */ 
    public String addMusicUrl(Long musicListId){
        return restProperties.getMusicServiceUrl()+"MusicList/musicToMusicList/"+musicListId;
    }

    /**
     * @description:  删除歌单歌曲
     * @param: [MusicListId, musicIdList]
     * @return: [MusicListId, musicIdList]
     * @author: WELSIR
     * @date: 2022/10/22 15:00
     */
    public String deleteMusicUrl(){
        return restProperties.getMusicServiceUrl()+"MusicList/musicInMusicList/";
    }

    /**
     * @description:  删除歌单
     * @param: [MusicListId]
     * @return: [MusicListId]
     * @author: WELSIR
     * @date: 2022/10/22 17:15
     */
    public String deleteMusicListUrl(Long MusicListId){
        return restProperties.getMusicServiceUrl()+"MusicList/MusicList/"+MusicListId;
    }

    /**
     * @description:  获取歌单信息
     * @param: [MusicListId]
     * @return: [MusicListId]
     * @author: WELSIR
     * @date: 2022/10/22 17:28
     */
    public String getMusicListInfo(Long MusicListId){
        return restProperties.getMusicServiceUrl()+"MusicList/MusicListInfo/"+MusicListId;
    }

    public String getUpdateMusicListInfo(){
        return restProperties.getMusicServiceUrl()+"MusicList/MusicList/";
    }

}
