package cn.tml.innermost.user.utils;

import cn.tml.innermost.framework.entity.enums.ResultCode;
import cn.tml.innermost.framework.exception.ServiceException;
import cn.tml.innermost.framework.security.AuthUser;
import cn.tml.innermost.framework.security.context.UserContext;
import cn.tml.innermost.user.dos.UserInfo;
import org.springframework.stereotype.Component;

/**
 * @author welsir
 * @date 2023/4/7 9:53
 */
@Component
public class UserInfoUtil {
    public UserInfo getUserInfo() {
        AuthUser authUser = UserContext.getCurrentUser();
        if (null == authUser) {
            throw new ServiceException(ResultCode.USER_NOT_LOGIN);
        }
        return UserInfo.valueOf(authUser);
    }
}
