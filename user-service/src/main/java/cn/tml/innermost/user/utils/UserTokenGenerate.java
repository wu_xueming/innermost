package cn.tml.innermost.user.utils;

import cn.tml.innermost.framework.security.AuthUser;
import cn.tml.innermost.framework.security.enums.UserEnums;
import cn.tml.innermost.framework.security.token.Token;
import cn.tml.innermost.framework.security.token.TokenUtil;
import cn.tml.innermost.framework.security.token.base.AbstractTokenGenerate;
import cn.tml.innermost.user.dos.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 用户Token生成器
 */
@Component
public class UserTokenGenerate extends AbstractTokenGenerate<UserInfo> {

    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public Token createToken(UserInfo user, Boolean longTerm) {
        AuthUser authUser = new AuthUser(user.getUsername(), user.getId(), UserEnums.USER, longTerm);
        return tokenUtil.createToken(authUser);
    }

    @Override
    public Token refreshToken(String refreshToken) {
        return tokenUtil.refreshToken(refreshToken);
    }
}